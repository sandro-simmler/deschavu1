<?php 

// config
require_once("inc/script/php/config.php");

// header
get_headerTemplate();

?>

  <div id="container">
  
      <!-- head -->
      <div id="head">
          
          <!-- logo & deko -->
          <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
          <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
          <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->
          
          <!-- adminLink -->
          <a id="loginLink" href="login.php">admin</a>
          
          <!-- menu -->
          <?php get_menuTemplate(); ?>
          
      </div>
      

      <div id="content">      	
              
			<!-- rules -->
			<div id="rules"> 
            
            	<h1>RULES</h1>
                <ol>
                	<li>Rules may change!! Unser Spiel dauert ein ganzes Jahr. Das ist zu lange, um die Regeln in Stein zu meisseln. Vieles kann passieren. Umweltkatastrophen, Terroranschl&auml;ge oder sonstiges kann uns zu &Auml;nderungen zwingen.</li>
                    <li>Das Spiel beginnt am 01.Novmenber 2013</li>
                    <li>Ablauf: Auf der Startseite von <a href="index.php">deschav&uuml;.ch</a> werden Fotos ver&ouml;ffentlicht, wenn du erkennst wo sich das darauf abgebildete Sujet befindet, kannst du das Bild anklicken und unter Angabe von Name und Ort mit "d&eacute;j&agrave;vu!" abschicken. Die erste richtige Antwort erh&auml;lt die angegebenen Punkte. Wer am 01.Novmeber 2014 die meisten Punkte hat, gewinnt. Um einen Tip abzugeben musst du dich unter <a href="play.php">"PLAY"</a> registrieren.</li>
                    <li>Bilder werden in Sets &agrave; ca. 10 St&uuml;ck zwischen Freitag 00:00 bis Samstag 24:00 hochgeladen. Es kann auch sein, dass zwischendrin einzelne Bilder spontan aufgeschaltet werden.</li>
                    <li>Punkte: Wird ein Bild in der ersten Woche nach Aufschaltung erkannt, so erh&auml;lt man 1 Punkt. Bleibt es nach einer Woche unerkannt, so erh&ouml;ht sich der Wert des Bildes jede weitere Woche um einen weiteren Punkt. (Es kann sich bei sehr schwierigen Bildern also lohnen, die Antwort nicht sofort zu geben, um mehr Punkte zu bekommen. Andererseits kann dann auch ein anderer Spieler zuvorkommen.)</li>
                    <li>Fundorte: Gebt die Fundorte m&ouml;glichst  genau und eindeutig an. Die Jury entscheidet, ob die Angaben genug genau sind. Bsp.: Antwort Hauptbahnhof ist wahrscheinlich in jedem Fall zu ungenau, Bahnhof Binz hingegen ist sicherlich ausreichend, wenn sich das gesuchte Sujet da befindet.</li>
                    <li>Ver&auml;nderungen: Die Stadt lebt und Sujets k&ouml;nnen sich &auml;ndern. Wir ein Sujet zur Unkenntlichkeit abge&auml;ndert oder verschwindet ganz, so nehmen wir das Bild 3-4 Wochen, nachdem wir von der &Auml;nderung Kenntnis haben aus dem Spiel.</li>
                    <li>Bei Punktegleichheit entscheidet die kleinere Anzahl gefundener Bilder.</li>
                    <li>Als richtig gelten einzig die Orte, an welchen das Foto tats&auml;chlich aufgenommen wurde. Will es der Zufall, dass an einem anderen Ort das genau gleiche Foto h&auml;tte entstehen k&ouml;nnen, so kann ein Foto mit gr&ouml;ssere Ausschnitt, an die Jury geschickt werden und es werden daf&uuml;r Zusatzpunkte vergeben. Dies gilt nur solange das gesuchte Foto nicht erkannt worden ist.</li>
                    <li>Fotos sind, wenn nichts anderes angegeben auf dem Gebiet der Stadt Z&uuml;rich.</li>
                    <li>Mit "AGGRO BONUS!!!" gekennzeichnete Bilder befinden sich in direkter Agglomeration der Stadt Z&uuml;rich (d.h. der Gemeindeort beginnt direkt an der Stadtgrenze von Z&Uuml;rich). Diese Bilder sind von Anfang an 2 Punkte wert und dann pro Woche 2 Punkte mehr.</li>
                    <li>Siegerehrung findet am 01.november 2014 irgendwo statt.</li>
                    <li>Der Rechtsweg ist ausgeschlossen.</li>
                </ol>
                
			</div>
		
      </div>
      
  </div>

<?php get_footerTemplate(); ?>