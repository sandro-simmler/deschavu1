<?php
//ini_set('display_errors', 1);
//ini_set('display_startup_errors', 1);
//error_reporting(E_ALL);
// config
require_once("inc/script/php/config.php");

// header
get_headerTemplate();

// loadOnScroll
$last_msg_id=$_GET['last_msg_id'];
$action=$_GET['action'];

if ($action <> "get") {

	?>
	
	  <div id="container">
	  
		  <!-- head -->
		  <div id="head">
			  
			  <!-- logo & deko -->
			  <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
			  <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
              <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->
			  
			  <!-- adminLink -->
			  <a id="loginLink" href="login.php">admin</a>
			  
			  <!-- menu -->
			  <?php get_menuTemplate(); ?>
			  
		  </div>
                  

		  <div id="content">      	
			<?php
			
				//if ($action <> "get") {
					// list entries
					$ENTRY->showFirstEntries("found");
				//} else {
					// list entries
					//$ENTRY->loadEntries("found");
				//}
			
			?>
		  </div>
		  
	  </div>
    
<?php get_footerTemplate(); 

} else {
		
	if (!$GLOBALS['MOBILE_DETECT']->isMobile()) {
		// list entries
		$ENTRY->loadEntries("found");
	} 

}

?>