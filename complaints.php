<?php 

// config
require_once("inc/script/php/config.php");

// header
get_headerTemplate();

?>

  <div id="container">
  
      <!-- head -->
      <div id="head">
          
          <!-- logo & deko -->
          <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
          <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
          <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->
          
          <!-- adminLink -->
          <a id="loginLink" href="login.php">admin</a>
          
          <!-- menu -->
          <?php get_menuTemplate(); ?>
          
      </div>
      
      <div id="content">      	
      		
            <div id="complaints">  
            
            <h1>complaints</h1>    	
      			
                <script type="text/javascript">
					// <![CDATA[
					mail_complaints("mail","taran","ch");
					// ]]>
       	 		</script>
                
        	</div>
      
      </div>
      
  </div>

<?php get_footerTemplate(); ?>