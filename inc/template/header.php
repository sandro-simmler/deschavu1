<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="de" xml:lang="de">

<head>

   <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta http-equiv="content-script-type" content="text/javascript" />
   <meta name="alleycat" content="DéjàVu" />

    <title>DéjàVu?!</title>
    
    <!-- favicon -->
    <link rel="shortcut icon" href="/inc/img/favicon/favicon.gif" type="image/x-icon" />
    
    <!-- SWF-Embeding -->
    <!-- SWFObject >> http://blog.deconcept.com/swfobject/ -->
    <script type="text/javascript" src="/inc/script/js/swfobject.js"></script>
    
    <!-- ShowAndHide Elements  -->
	<script type="text/javascript" src="/inc/script/js/showHide.js"></script>

    
    <!-- CSS_link -->
    <link rel="stylesheet" href="<?php echo CSS_DIR  ?>style01.css" type="text/css" title="Default Style" media="screen" />
    <link rel="stylesheet" href="<?php echo CSS_DIR  ?>print01.css" type="text/css" title="Default Style" media="print" />


	<!--[if IE 8]>
  		<link rel="stylesheet" href="<?php echo CSS_DIR  ?>ie8-fix.css" type="text/css" title="Default Style" media="screen" />
  	<![endif]-->
    <!--[if lt IE 8]>
  		<link rel="stylesheet" href="<?php echo CSS_DIR  ?>ie7-fix.css" type="text/css" title="Default Style" media="screen" />
  	<![endif]-->



	<?php $GLOBALS['XAJAX']->printJavascript(); ?>

	<script type="text/javascript">
	<!--//--><![CDATA[//><!--
    function submitUpdate() {
        xajax.$('submitButton').disabled=true;
        xajax.$('submitButton').value="please wait...";
        xajax_processForm(xajax.getFormValues("updateForm"));
        return false;
    }
	//--><!]]>
    </script>

	<!-- jQuery -->
	<script type="text/javascript" src="ttps://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <!--<script type="text/javascript" src="inc/script/js/jquery-1.2.6.pack.js"></script>-->
    
    <!-- loadOnScroll -->
    <script type="text/javascript" src="/inc/script/js/jquery.json-2.2.min.js"></script>
    <script type="text/javascript" src="/inc/script/js/loadOnScroll.js"></script>
    
	<!-- antiSpam Mail -->
    <script type="text/javascript" src="/inc/script/js/mail.js"></script>

	<script type="text/javascript">
	<!--//--><![CDATA[//><!--
					  
	function confirmSubmit(id, subject) {
		var agree=confirm("wötsch das würkli lösche?");
		if (agree) {
			if (subject=="entry") {
				xajax_deleteEntry(id);
			}
			if (subject=="user") {
				xajax_deleteUser(id);
			}
			if (subject=="player") {
				xajax_deletePlayer(id);
			}
			//return true ;
		} else {
			return false ;
		}
	}
	


					  
					  
	// tableLink color
	/*function ChangeColor(div, highLight) {
			if (highLight) {
				div.style.color = '#fedfa3';
			} else {
		  		div.style.color = 'white';
			}
	  	}  
	*/
	/*
	
	  $(".all_entriesTEXT").mouseover(function() {
											   alert($(this));
    	$(this).find("a").this.style.color = "blue";
  		}).mouseout(function(){
    	$(this).find("a").this.style.color = "white";
  	});
*/
	
	/*
	$(document.body).click(function () {
									 
									 
									 
      $("div").each(function (i) {
        if (this.style.color != "blue") {
          this.style.color = "blue";
        } else {
          this.style.color = "";
        }
		
		
      });
	  */


	//--><!]]>
	</script>



</head>
<body <?php global $admin; if ($admin) { echo 'id="admin"'; } ?>>

<img id="page-curl" src="/inc/img/content/logo/deschavue02_pageCurl-vol2b.png" alt="Volume #2" />