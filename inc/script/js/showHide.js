// Function to show or hide Elements
function show(id)
{
	with (document)
	{
		if (getElementById)
			getElementById(id).style.display = 'block';
		else if (all)
			all[id].style.display = 'block';
		else
			layers[id].display = 'block';
	}
}

function hide(id)
{
	with (document)
	{
		if (getElementById)
			getElementById(id).style.display = 'none';
		else if (all)
			all[id].style.display = 'none';
		else
			layers[id].display = 'none';
	}
}