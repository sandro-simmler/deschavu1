var ID=$(".message_box:last").attr("id");
var oldID=0;
var loadContent=false;
var loadedID=[];

$(document).ready(function() {
						
	//
	function last_msg_funtion() {
		if (loadContent==true) {
			ID=$(".message_box:last").attr("id");
			$('div#last_msg_loader').html('<img src="inc/img/content/bigLoader.gif">');
			
			/*
			$.each(loadedID, function(index, value) { 
				//alert(index + ': ' + value); 
			  	//alert($.toJSON(loadedID));
			    //alert(ID + ' : ' + value);
			  
				if (ID==value) {
					alert(ID + ' : ' + value);
				}
				
				var valueBefore=loadedID[index-1];
				if (value==valueBefore) {
					alert(value + ' : ' + valueBefore);
				}
			});
			// add ID to array
			loadedID.push([ID]);
			*/
			
			var pathname = window.location.pathname;
			if (pathname=="/dejavu" || pathname=="/dejavu.php") {
				//$.post("load_data.php?action=get&last_msg_id="+ID,
				$.post("dejavu.php?action=get&last_msg_id="+ID,
				function(data) {
					if (data != "") {
						$(".message_box:last").after(data);
						loadContent=false;
					}
						$('div#last_msg_loader').empty();
				});
			} else {
				$.post("index.php?action=get&last_msg_id="+ID,
				function(data) {
					if (data != "") {
						$(".message_box:last").after(data);
						loadContent=false;
					}
						$('div#last_msg_loader').empty();
				});
			}
			
			oldID=$(".message_box:last").attr("id");
			
		}
		
	};
		
		
	if (loadContent==false) {
				
		$(window).scroll(function() {
			
			ID=$(".message_box:last").attr("id");
	
			if ($(window).scrollTop() == $(document).height() - $(window).height() && ID!=oldID) {
				if (ID>1) {
					loadContent=true;
					last_msg_funtion();
				} 
			}	
		
		});
	}

    
});


/*

$(document).ready(function(){
var fetching = false;

function lastPostFunc()
{
fetching = true;
$(‘div#lastPostsLoader’).html(”);
$.post(“../user/load2.php?lastID=”+$(“.comment:last”).attr(“id”),

function(data){
if (data != “”) {
$(“.comment:last”).after(data);
setTimeout(function(){
fetching = false;
},300);
$(‘div#lastPostsLoader’).empty();
}
});
};

$(window).scroll(function(){
var bufferzone = $(window).scrollTop() * 0.20;
if (!fetching && ($(window).scrollTop() + bufferzone > ($(document).height()- $(window).height() ) )){
lastPostFunc();
}
});
});

*/