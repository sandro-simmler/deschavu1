<?php
use Jaxon\Response\Response;
use Jaxon\Jaxon;

//error Reporting
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

// Absoluter Pfad (Root-Verzeichnis)
$absPathConfig=str_replace("/inc/script/php", "", dirname(__FILE__));
define('ABS_PATH', $absPathConfig);

// Webadressierung
define('HTTP_ROOT', "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']));

// Template Pfad
define('TPL_PATH', ABS_PATH."/inc/template");

// modules Verzeichnis
define('MODULES_DIR', ABS_PATH."/inc/script/php/modules/");

// CSS Verzeichnis
define('CSS_DIR', HTTP_ROOT."inc/script/css/");

// Upload Verzeichnis
define('UPL_DIR', "/var/www/inc/img/uploads/");

// Upload Verzeichnis (Webadressierung)
define('UPL_DIR_WEB', "/inc/img/uploads/");

// Xajax Pfad
//define('XAJAX_PATH', HTTP_ROOT."/inc/script/xajax/");
define('XAJAX_PATH', "inc/script/ajax/xajax/");


// Systemeinstellungen (z.B. Datenbanksettings)
require_once('inc/script/php/settings.php');

// Klassen und Funktionen einbinden
require_once('inc/script/php/includes.php');

// Global verfügbare Objekte

$DB = new DB();

$LOGIN = new login();

$REGISTER = new register();
$PLAYER = new player();

$SESSION_HANDLER = new sessionHandler();

$ENTRY = new entry();
$IMGUPLOAD = new imgUpload();

$FINDIMG = new findImg();

$MOBILE_DETECT = new Mobile_Detect();

$XAJAX = new xajax();
$XAJAX->registerFunction("editEntry");
$XAJAX->registerFunction("deleteEntry");
$XAJAX->registerFunction("editPlayer");
$XAJAX->registerFunction("deletePlayer");
$XAJAX->registerFunction("processForm");
$XAJAX->processRequests();


//JAXON
//$jaxon = new jaxon();
//$jaxon->register(Jaxon::CALLABLE_FUNCTION, "ajax_test");
//$jaxon->processRequest();
