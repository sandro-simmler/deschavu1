<?

define('CODE_WIDTH',         780);
define('CODE_HEIGHT',        280);
define('CODE_FONT_SIZE',     60);
define('CODE_CHARS_COUNT',   5);
define('CODE_LINES_COUNT',   60);
define('CODE_CHAR_COLORS',   "#000000");
define('CODE_LINE_COLORS',   "#99ffff,#66ccff,#0099ff,#ff9933,#ff9900,#ffcc66,#ffffff");
define('CODE_BG_COLOR',      "#FFFFFF");
define('CODE_ALLOWED_CHARS', "ABCDEFGHJKLMNPQRSTUVWXYZ");
define('PATH_TTF',           dirname(__FILE__)."/fonts");

?>