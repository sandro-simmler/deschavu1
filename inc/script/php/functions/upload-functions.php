<?php

function eintragAusfuehren($eintragDaten) {
   
            // Ueberpruefen ob Bild hochgeladen werden soll
            if ($_FILES['bildfile']['tmp_name']!=null) {
               // Ueberprufen ob das Bildfile im richtigen Format und nicht zu gross ist
               $allowedImgExtensions = array("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "gif", "GIF");
               //$eintragAusfuehren[0] = $GLOBALS['ENTRYIMG']->img_check($_FILES['bildfile']['name'], $_FILES['bildfile']['size'], $allowedImgExtensions);
               list($returnValue, $errorMessage) = $GLOBALS['ENTRYIMG']->img_check($_FILES['bildfile']['name'], $_FILES['bildfile']['tmp_name'], $_FILES['bildfile']['size'], $allowedImgExtensions);
               // falls ein Fehler vorhanden - Meldung und zurueck
               if ($returnValue==false) {
                  return array(false, $errorMessage, false);	
               }      
            } 
     
            // Ueberpruefen ob Sound hochgeladen werden soll
            if ($_FILES['soundfile']['tmp_name']!=null) {
               // Ueberprufen ob das Soundfile im richtigen Format und nicht zu gross ist
               $allowedSndExtensions = array("mp3", "MP3", "mP3", "Mp3", "wav", "WAV", "wave", "WAVE");               
               // $allowedSndExtensions = array("mp3", "MP3", "mP3", "Mp3");               
               list($returnValue, $errorMessage) = $GLOBALS['ENTRYSND']->snd_check($_FILES['soundfile']['name'], $_FILES['soundfile']['size'], $allowedSndExtensions);
               // falls ein Fehler vorhanden - Meldung und zurueck
               if ($returnValue==false) {
                  return array(false, $errorMessage, false);	
               }
            }
            
            // mailCheck!!!
            if ($eintragDaten[1]!="") {
               if (!check_email($eintragDaten[1])) {
               
                  $errorMessage="Sorry, aber etwas ist mit deiner Mail Adresse nicht ok..";
                  return array(false, $errorMessage, false);	
               
               }
            }
            
            // website Check!!!
            if ($eintragDaten[2]!="") {
              
               $webArray=explode("://", trim($eintragDaten[2]));     
               // no http://
               if (sizeof($webArray)<=1) {
                  $eintragDaten[2]="http://".$eintragDaten[2];
               }
                        
               if (!http_test_existance($eintragDaten[2])) {

                   $errorMessage="Sorry, aber etwas ist mit deiner Webpage nicht ok.. Versuch mal die ganze URL einzugeben.";
                   return array(false, $errorMessage, false);	

                }
             
            }

            // eintraege schreiben
            list($returnValue, $returnMessage, $eintragResultatID) = $GLOBALS['ENTRIES']->add_entry($eintragDaten);       
            if($returnValue) {
   		 
               // Text Eintrag
               list($returnValue, $returnMessage) = $GLOBALS['ENTRYTXT']->add_textEntry($eintragResultatID, $eintragDaten[4]);
               if ($returnValue==FALSE) {
                  return array(false, $returnMessage, $eintragResultatID);	
               }               
            
               // BildEintrag
               // Ueberpruefen ob Bild hochgeladen werden soll
               if ($_FILES['bildfile']['tmp_name']!=null) {
               
                  // Bildeintrag schreiben
                  list($returnValue, $returnMessage) = $GLOBALS['ENTRYIMG']->add_imgEntry($eintragResultatID, $eintragDaten[5], $eintragDaten[7]);
                  
                  if ($returnValue==FALSE) {
                     return array(false, $returnMessage, $eintragResultatID);	
                  }
               }
           
               // SoundEintrag
               // Ueberpruefen ob Sound hochgeladen werden soll
               if ($_FILES['soundfile']['tmp_name']!=null) {
               
                  // Soundeintrag schreiben
                  list($returnValue, $returnMessage) = $GLOBALS['ENTRYSND']->add_sndEntry($eintragResultatID, $eintragDaten[6], $eintragDaten[7]);
                  if ($returnValue==FALSE) {
                     return array(false, $returnMessage, $eintragResultatID);	
                  }
               }
   					$message = 'Eintrag gespeichert';
                  return array(true, $message, false);
   				} else {
                                    
                  return array(false, $returnMessage, false);	
   			}
   				
} // END eintragAusfuehren











?>