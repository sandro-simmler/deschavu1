<?php

// imgValidation - antiSpam - Variante 01
function imgVal01() {

   // bild generieren
   list($code, $codeLowLetter, $ImageDataEnc) = imgValCreate01();

   ?>

   <!-- imgCheck form -->
   <div id="imgVal">

	<form action="<?php echo HTTP_ROOT.'/gbuch.php?getRequest=entry' ?>" method="post">

     <img id="__code__" src="data:image/gif;base64,<?=$ImageDataEnc?>" style="border:1px solid #000000"  alt="captcha" />
     <br/>
     <a href="gbuch.php">Zur&#252;ck</a>
     <br/>
     <p>Bitte Nummer auf dem Bild eingeben:</p>
     <input name="checkcode" type="text" id="checkcode" size="5" />

     <input name="codemd5Up" type="hidden" id="codemd5Up" value="<?=md5($code)?>" />
     <input name="codemd5Low" type="hidden" id="codemd5Low" value="<?=md5($codeLowLetter)?>" />

     <input type="submit" value="Check!" />
   </form>

   </div>

   <div id="imgVal_new">
      <a href="gbuch.php?getRequest=entry">Neuen Code erzeugen</a>
   </div>

   <?php

} // END imgVal()

// imgValidation - antiSpam - Variante 02
function imgVal02() {

   // bild generieren
   list($code, $ImageDataEnc) = imgValCreate02();

?>
            <div id="imgVal_mail">

               <form action="<?php echo HTTP_ROOT.'/login.php?getRequest=mail' ?>" method="post">

            	     <p>Bitte Nummer eingeben.</p>

            	     <img id="__code__" src="data:image/gif;base64,<?=$ImageDataEnc?>" style="border:1px solid #000000" alt="captcha" />
            	     <br/>
            	     <input name="checkcode" type="text" id="checkcode" size="5" />
                    <br/>

                  <input name="codemd5" type="hidden" id="codemd5" value="<?=md5($code)?>" />
                  <p><input type="submit" value="Check!" /></p>

                  <a href="login.php?getRequest=mail">Neuen Code erzeugen</a>

                </form>

             </div>

   <?php

} // END imgVal()

// imgVal Functions
function imgValCreate01() {

   require_once(MODULES_DIR."imgval01/_config.php");

   $line_colors = preg_split("/,\s*?/", CODE_LINE_COLORS);
   $char_colors = preg_split("/,\s*?/", CODE_CHAR_COLORS);
   $fonts = collect_files(PATH_TTF, "ttf");

   $img = imagecreatetruecolor(CODE_WIDTH, CODE_HEIGHT);
   imagefilledrectangle($img, 0, 0, CODE_WIDTH - 1, CODE_HEIGHT - 1, gd_color(CODE_BG_COLOR));

   // Draw boxes
   for ($i = 0; $i < CODE_LINES_COUNT; $i++) {
       imagefilledrectangle($img,
           rand(-20, CODE_WIDTH - 1),
           rand(-20, CODE_HEIGHT - 1),
           rand(-20, CODE_WIDTH - 1),
           rand(-20, CODE_HEIGHT - 1),
           gd_color($line_colors[rand(0, count($line_colors) - 1)])
       );
   }

   // Draw code
   $code = "";
   $y = (CODE_HEIGHT / 2) + (CODE_FONT_SIZE / 2);
   for ($i = 0; $i < CODE_CHARS_COUNT; $i++) {
       $color = gd_color($char_colors[rand(0, count($char_colors) - 1)]);
       $angle = rand(-30, 30);
       $char = substr(CODE_ALLOWED_CHARS, rand(0, strlen(CODE_ALLOWED_CHARS) - 1), 1);
       $font = PATH_TTF . "/" . $fonts[rand(0, count($fonts) - 1)];
       $x = (intval((CODE_WIDTH / CODE_CHARS_COUNT) * $i) + (CODE_FONT_SIZE / 2)) + 10;
       $code .= $char;
       imagettftext($img, CODE_FONT_SIZE, $angle, $x, $y, $color, $font, $char);

   }

   $codeLowLetter=strtolower($code);

   $filenametemp=MODULES_DIR."imgval01/tmp/gif".time().".gif";
   ImageGIF($img, $filenametemp);
   $ImageData = file_get_contents($filenametemp);
   $ImageDataEnc = base64_encode($ImageData);
   unlink($filenametemp); // delete the file

   return array($code, $codeLowLetter, $ImageDataEnc);

}

function gd_color($html_color) {
    return preg_match('/^#?([\dA-F]{6})$/i', $html_color, $rgb)
      ? hexdec($rgb[1]) : false;
}


function collect_files($dir, $ext) {
    if (false !== ($dir = opendir($dir))) {
        $files = array();

        while (false !== ($file = readdir($dir)))
            if (preg_match("/\\.$ext\$/i", $file))
                $files[] = $file;

        return $files;

    } else
        return false;
}

function imgValCreate02() {

   // create a 88*31 image
   $img = imagecreate(88, 31);

   // random colored  background and   text
   $bg = imagecolorallocate($img, rand(0,255) , rand(0,255), rand(0,255));
   $textcolor = imagecolorallocate($img, 0, 0, rand(0,255));

   // write the random 4 digits number  at a random locaton (x= 0-20, y=0-20),
   $code=rand(1000,9999);
   imagestring($img, rand(1,8), rand(0,20), rand(0,20), $code , $textcolor);

   // write  the image to a temporary file , in this case it is 777 chmoded tmp folder in same directory
   // could be a generic /tmp folder
   $filenametemp=MODULES_DIR."imgval01/tmp/".time().".gif";

   //$filenametemp=ABS_PATH."/inc/script/php/modules/imgval01/tmp/".time().".gif";

   imagegif($img, $filenametemp);
   $ImageData = file_get_contents($filenametemp);
   $ImageDataEnc = base64_encode($ImageData);
   unlink($filenametemp); // delete the file

   return array($code, $ImageDataEnc);

}





// Display entryForm
function displayForm($entryname, $email, $webpage, $text, $sndname, $soundfile, $imgname, $bildfile, $output) {

   ?>

	   <div id="gb_eintrag">

	      <h3>Neuer Eintrag</h3>
         <form enctype="multipart/form-data" action="<?php echo HTTP_ROOT.'/gbuch.php?getRequest=add' ?>" method="post" accept-encoding="UTF-8">

            <div id="gb_eintrag_name">
      	   	<label for="entryname">Name *</label><br />
         		<input type="text" id="entryname" name="entryname" value="<?php echo $entryname ?>" />
      		</div>

            <br/>

            <div id="gb_eintrag_mail">
         		<label for="email">Email</label><br />
         		<input type="text" id="email" name="email" value="<?php echo $email ?>" />
      		</div>

            <br/>

            <div id="gb_eintrag_web">
         		<label for="webpage">Webpage</label><br />
         		<input type="text" id="webpage" name="webpage" value="<?php echo $webpage ?>" />
      		</div>

            <br/>

            <div id="gb_eintrag_text">
         		<label for="text">Text *</label><br />
         		<textarea name="text" id="text" rows="12" cols="58" ><?php echo $text ?></textarea>
      		</div>

            <br/>

            <div id="gb_eintrag_imgname">
               <label for="imgname">Bildname</label><br />
         		<input type="text" id="imgname" name="imgname" value="<?php echo $imgname ?>" />
            </div>

      		<div id="gb_eintrag_img">
         		<label for="userfile">Bild hochladen</label><br />
         		<p>(JPG - PNG - GIF &lowast; max. 1MB)</p>
         		<input type="file" id="userfile" name="bildfile" value="<?php echo $bildfile ?>" /><br />
      		</div>

            <div id="gb_eintrag_sndname">
         		<label for="sndname">Soundname</label><br />
         		<input type="text" id="sndname" name="sndname" value="<?php echo $sndname ?>" />
   		   </div>

            <div id="gb_eintrag_snd">
               <label for="userfile">Sound hochladen</label><br />
               <p>(MP3 - WAV &lowast; max. 2MB)</p>
               <!-- <label for="userfile">Sound ausw&auml;hlen... (.mp3 // max. 2MB)</label><br /> -->
         		<input type="file" id="userfile" name="soundfile" value="<?php echo $soundfile ?>" /><br />
   		   </div>

            <div id="gb_eintrag_button">
         		<input type="submit" name="action" value="Speichern" />
      		</div>

      	</form>

      </div>

      <div id="gb_eintrag_backLink">
         <a href="gbuch.php">Zur&#252;ck</a>
      </div>


   <?php

   echo $output;

}


// displayEntries
function displayEntries() {

   ?>

	<div id="gb_entries">
		<?php
		// Eintraege aus DB lesen
		getEntries();
		?>
	</div>

	<?php

} // END displayEntries


// DATE FORMATTING
function DatumsWandler($Datum) {

    if (strlen($Datum) == 10) {

        $GewandeltesDatum = substr($Datum, 8, 2);
        $GewandeltesDatum .= ".";
        $GewandeltesDatum .= substr($Datum, 5, 2);
        $GewandeltesDatum .= ".";
        $GewandeltesDatum .= substr($Datum, 0, 4);
        return $GewandeltesDatum;

    } else if (strlen($Datum) == 19) {
		$GewandeltesDatum = substr($Datum, 8, 2);
		$GewandeltesDatum .= ".";
		$GewandeltesDatum .= substr($Datum, 5, 2);
		$GewandeltesDatum .= ".";
		$GewandeltesDatum .= substr($Datum, 0, 4);
		$GewandeltesDatum .= " - ";
		$GewandeltesDatum .= substr($Datum, 10);
		return $GewandeltesDatum;
	} else {
        return FALSE;
    }
}

function setDate01($date) {
	$format = substr($date, 0, -9);
	$format = explode("-", $format);
	$format = $format[2].".".$format[1].".".substr($format[0], -2);
	//$date->format('d.m.y');
	return $format;
}
function setDate02($date) {
	$format = substr($date, 0, -9);
	//$format = explode("-", $format);
	//$format = $format[2]."-".$format[1]."-".substr($format[0]);
	//$date->format('Y.m.d');
	return $format;
}

// time difference
function daysDifference01($endDate, $beginDate) {
   //explode the date by "-" and storing to array
   $date_parts1=explode("-", $beginDate);
   $date_parts2=explode("-", $endDate);
   //gregoriantojd() Converts a Gregorian date to Julian Day Count
   $start_date=gregoriantojd($date_parts1[1], $date_parts1[2], $date_parts1[0]);
   $end_date=gregoriantojd($date_parts2[1], $date_parts2[2], $date_parts2[0]);
   return $end_date - $start_date;
}
function daysDifference02($endDate, $beginDate) {
	$beginDate = strtotime($beginDate);
	$endDate = strtotime($endDate);
	$diff = $endDate - $beginDate;
	$diff = ceil($diff / (60*60*24)) ;
	return $diff;
}



// Rename Functions
// get extension
function isAllowedExtension($fileName, $allowedExtensions) {
  return in_array(end(explode(".", $fileName)), $allowedExtensions);
}

//
function renameTimestamp($filename, $timestamp) {
   // this applies the function to our file
   list($ext, $unExt) = findexts($filename, true);

   // this takes the random number (or timestamp) you generated and adds a . on the end, so it is ready of the file extension to be appended.
   $newFileName = $unExt . $timestamp. ".".$ext;

   // return newFilename
   return $newFileName;
}

// this function separates the extension from the rest of the file name and returns both
function findexts($filename, $underline) {
   $filename = strtolower($filename) ;
   $exts = explode("[/\\.]", $filename) ;

   // get arrayLength -1
   $n1 = count($exts)-1;

   // get unextented name
   $unExts = "";
   if ($underline) {
      for($i=0; $i<$n1; $i++) {
         $unExts .= $exts[$i]."_";
      }
   } else {
      for($i=0; $i<$n1; $i++) {
         $unExts .= $exts[$i];
      }
   }

   // get extension
   $exts = $exts[$n1];

   return array($exts, $unExts);
}

function shortenText($text, $number) {

    // Change to the number of characters you want to display
    $chars = $number;

    $text = $text." ";
    $text = substr($text,0,$chars);
    $text = substr($text,0,strrpos($text,' '));
    $text = $text."...";

    return $text;

}

// Round Number
function roundPrecision($value, $precision=2 ){
    $round = $precision - floor(log10(abs($value))) - 1;
    return round($value, $round);
}






//
// CHECK URL FUNCTION
//***********************
function http_test_existance($url) {
 return (($fp = @fopen($url, 'r')) === false) ? false : @fclose($fp);
}

//
// CHECK DONATION FUNCTION
//***********************
function checkSpende($spende) {

  $spendeArray = explode(".",$spende);
  if (sizeof($spendeArray)>2) {
     return false;
  }

  $spende = str_replace(".","",$spende);

  if (preg_match("/^[0-9]+$/", $spende )) {
      return true;
  } else {
     return false;
  }
}


// REQUEST check
function newInput($in,$error=0) {
    $in = trim($in);
    if (strlen($in))
    {
        $in = htmlspecialchars($in);
        $in = preg_replace('/&amp;(\#[0-9]+;)/','&$1',$in);
    }
    elseif ($error)
    {
        problem($error);
    }
    return stripslashes($in);
} // END newInput()


function checkName($name) {

   // if (preg_match('/[0-9]/', $name)) {
   //
   //    }
   return true;

}


//
function isNumber($number) {
	if (!is_numeric($number)) {
		return "keine nummer";
	} else {
		return NULL;
	}
}
function isNoLetter($in) {

	if (is_numeric($in)) {
		return TRUE;
	}
	if (!preg_match("/[a-zA-Z]/", $in)) {
		return TRUE;
	}

	return FALSE;

}
?>
