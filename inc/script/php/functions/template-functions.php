<?php

function get_headerTemplate()
{
	include(TPL_PATH."/header.php");
}

function get_menuTemplate()
{
  include(TPL_PATH."/menu.php");
}

function get_adminMenu()
{
  include(TPL_PATH."/adminMenu.php");
}

function get_footerTemplate()
{
	include(TPL_PATH."/footer.php");
}

?>