<?php

	// DB-Klasse
	require_once(ABS_PATH."/inc/script/php/classes/class-db.php");

	// Sicherheitsklasse
//	require_once(ABS_PATH."/inc/script/php/classes/class-security.php");

	// Session-Handler
	require_once(ABS_PATH."/inc/script/php/classes/class-session-handler.php");

	// Loginklasse
	require_once(ABS_PATH."/inc/script/php/classes/class-login.php");

	// Regsiter-Klassen
	require_once(ABS_PATH."/inc/script/php/classes/class-register.php");
	require_once(ABS_PATH."/inc/script/php/classes/class-player.php");

	// Entries-Klassen
	require_once(ABS_PATH."/inc/script/php/classes/class-entry.php");
	require_once(ABS_PATH."/inc/script/php/classes/class-imgUpload.php");

	//
	require_once(ABS_PATH."/inc/script/php/classes/class-findImg.php");

	// mobile detection
	require_once(ABS_PATH."/inc/script/php/classes/Mobile_Detect.php");

	// Xajax
	require_once(ABS_PATH."/inc/script/ajax/xajax/xajax.inc.php");

	//Composer Autoload
	//require_once(ABS_PATH."/vendor/autoload.php");


    // General Functions
	require_once(ABS_PATH."/inc/script/php/functions/functions.php");
	require_once(ABS_PATH."/inc/script/php/functions/upload-functions.php");

	require_once(ABS_PATH."/inc/script/php/functions/email.php");

	require_once(ABS_PATH."/inc/script/php/functions/template-functions.php");
	require_once(ABS_PATH."/inc/script/ajax/xajax/xajax-functions.php");

?>
