<?php

class imgUpload {

	public function __construct(){

		$this->DB = $GLOBALS['DB'];

	}

	//// check data
	// extension
	public function checkExtension($imgName, $allowedImgExtensions) {
      	$allowedFile = isAllowedExtension($imgName, $allowedImgExtensions);
      	if ($allowedFile==FALSE) {
         	return 'unerlaubtes file-format beim bild-upload';
		}
	}

	// size
	public function checkSize($imgSize, $allowedImgSize) {
		// ImgFile Size pruefen < 1'000'000 byte
        if ($imgSize > $allowedImgSize) {
            return 'bild file ist zu gross';
		}
	}

	// measure
	public function checkMeasure($imgTmpName, $allowedImgMeasure) {
		// Laenge und Breite des Bildes pruefen
        list($width, $height, $type) = getimagesize($imgTmpName);

		// png check
		if ($type==3) {
		   if ($width > $allowedImgMeasure[0]) {
			  return 'sry! die breite des bildes ist zu gross';
		   }
		   if ($height > $allowedImgMeasure[1]) {
			  return 'sry! die h&ouml;he des bildes ist zu gross';
		   }
		}

	   if ($width > $allowedImgMeasure[2]) {
		  return 'sry! die breite des bildes ist zu gross';
	   }
	   if ($height > $allowedImgMeasure[3]) {
		  return 'sry! die h&ouml;he des bildes ist zu gross';
	   }
	}

	//// uploadIMG
	public function uploadIMG($imgName, $uploadPath) {

	  $uploaddirTMP_original = $uploadPath.'original/';
	  $uploaddirTMP_thumb = $uploadPath.'thumb/';
	  $uploaddirTMP_thumb_deschavu = $uploadPath.'thumb_deschavu/';
	  $uploaddirTMP_thumbMini = $uploadPath.'thumbMini/';

	  $thumb_width = 610;
	  $thumb_height = 99999; // -> go for the width!

	  $thumb_deschavu_width = 260;
	  $thumb_deschavu_height = 99999; // -> go for the width!

	  $thumb_widthMini = 100;
	  $thumb_heightMini = 9999; // -> go for the width!

	  // testen ob file uploaded ist
	  if (is_uploaded_file($_FILES['bild_file']['tmp_name'])) {

		  // Testen ob ein Thumb erstellt worden ist
		  if($this->imageThumbs($imgName, $uploaddirTMP_thumb, $thumb_width, $thumb_height) == TRUE) {
		  if($this->imageThumbs($imgName, $uploaddirTMP_thumb_deschavu, $thumb_deschavu_width, $thumb_deschavu_height) == TRUE) {
		  if($this->imageThumbs($imgName, $uploaddirTMP_thumbMini, $thumb_widthMini, $thumb_heightMini) == TRUE) {

			  // Bild Name mit TimeStamp, dass keine gleichnamigen Bilder überschrieben werden (getTime), eindeutiger Name
			  // File in Bildordner verschieben
			  if (move_uploaded_file($_FILES['bild_file']['tmp_name'], $uploaddirTMP_original.$imgName)) {
				 $returnMessage = '';
				 return $returnMessage;

			  } else {
				 $returnMessage = "konnte bild nicht verschieben!";
				 return $returnMessage;

			  }

		  } else {
			  $returnMessage = "konnte kein thumbnail generieren!";
			  return $returnMessage;
		  }

		  } else {
			  $returnMessage = "konnte kein thumbnail generieren!";
			  return $returnMessage;
		  }

		  } else {
			  $returnMessage = "konnte kein thumbnail generieren!";
			  return $returnMessage;
		  }

	  } else {

		  // fehlermeldung beim dateiupload
		  $returnMessage = $this->uploadImg_error_handling($_FILES['bild_file']['error']);
		  $returnMessage .= ' img_error_handling';
		  return $returnMessage;
	  }

	}

	//// moveIMG
	public function moveIMG($imgName, $currentPath, $destinationPath) {
		$filePath_thumb = $currentPath."thumb/".$imgName;
		$filePath_thumbMini = $currentPath."thumbMini/".$imgName;
		$filePath_original = $currentPath."original/".$imgName;
		$filePath_thumb_deschavu = $currentPath."thumb_deschavu/".$imgName;

		$destinationPath_thumb = $destinationPath."thumb/".$imgName;
		$destinationPath_thumbMini = $destinationPath."thumbMini/".$imgName;
		$destinationPath_original = $destinationPath."original/".$imgName;
		$destinationPath_thumbDeschavu = $destinationPath."thumb_deschavu/".$imgName;

		rename($filePath_thumb, $destinationPath_thumb);
		rename($filePath_thumbMini, $destinationPath_thumbMini);
		rename($filePath_original, $destinationPath_original);
		rename($filePath_thumb_deschavu, $destinationPath_thumbDeschavu);
	}


   //// Bild-Upload Fehler-Tabelle
   public function uploadImg_error_handling($error)
	{
		switch($error){
			case 0:
					$errorMessage = "Es liegt kein Fehler vor, die Datei wurde erfolgreich hochgeladen.";
					return $errorMessage;
			break;
			case 1:
					$errorMessage = "Die hochgeladene Datei überschreitet die in der Anweisung upload_max_filesize in php.ini festgelegte Größe.";
					return $errorMessage;
			break;
			case 2:
					$errorMessage = "Die hochgeladene Datei überschreitet die in dem HTML Formular mittels der Anweisung MAX_FILE_SIZE angegebene maximale Dateigröße.";
					return $errorMessage;
			break;
			case 3:
					$errorMessage = "Die Datei wurde nur teilweise hochgeladen.";
					return $errorMessage;
			break;
			case 4:
					$errorMessage = "Es wurde keine Datei hochgeladen.";
					return $errorMessage;
			break;
			default:
					$errorMessage = "Unbekannter Fehler beim Dateiupload.";
					return $errorMessage;
			break;
		}
	}

   //// thumbnail
   public function imageThumbs($imgName, $uploaddirTMP_thumb, $width, $height) {

		// set tmp_name
		$filename01 = $_FILES['bild_file']['tmp_name'];

		// set upload name and target
		$target = $uploaddirTMP_thumb . $imgName;

		// get dimension
		list($width_orig, $height_orig, $type) = getimagesize($filename01);

		$ratio_orig = $width_orig/$height_orig;

		if ($width/$height > $ratio_orig) {
		   $width = $height*$ratio_orig;
		} else {
		   $height = $width/$ratio_orig;
		}

		// resample
		$image_p = imagecreatetruecolor($width, $height);
		if ($type==2) {
		   $image = imagecreatefromjpeg($filename01);
		} else if ($type==3) {
			 $image = imagecreatefrompng($filename01);
		} else if ($type==1) {
		   $image = imagecreatefromgif($filename01);
		} else {
		  return false;
		}

		// copy img to new created imageFile
		$imgResampled = imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);

		if(imagejpeg($image_p, $target, 100)) {
			return true;
		} else {
			return false;
		}

   }
}

?>
