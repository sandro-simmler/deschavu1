<?php

class player {
	
	
	public function __construct() {
		
		$this->DB = $GLOBALS['DB'];
		
	}
	
	//// new player
	public function writePlayerData() {
						
	  // returnMessage variable
	  $returnMessage = "";
		
	  //// pruefe $_POST Variablen
	  // name
	  if (isset($_POST['playerName'])) {
			$formVars["playerName"] = trim(htmlentities($_POST['playerName'], ENT_QUOTES, "UTF-8"));
			if ($formVars["playerName"]=="") {
				$returnMessage["playerName"] = "player eingeben!";
			}
	   } 
	   // race-ID
	   if (isset($_POST['raceID'])) {
			 $formVars["raceID"] = trim(htmlentities($_POST['raceID'], ENT_QUOTES, "UTF-8"));
			if ($formVars["raceID"]=="") {
				$returnMessage["raceID"] = "RACE-ID eingeben!";
			} else if (!isNoLetter($formVars["raceID"])) {
				$returnMessage["raceID"] = "nur nummer eingeben!";	
			}
	   } 
	   // mail
	   if (isset($_POST['email'])) {
			$formVars["email"] = htmlentities($_POST['email'], ENT_QUOTES, "UTF-8");
			if ($formVars["email"]!="" && !check_email($formVars["email"]) && spamcheck($formVars["email"])) {
				$returnMessage["email"] = "mail falsch!";
			}
	   } 
	   
	   if ($returnMessage!=NULL) {
			//// return data	   
			return array($formVars, $returnMessage);	
	   } else {
		  // pruefe doppelte eintraege
		  $spieler = $this->DB->query("SELECT * FROM spieler");
		  foreach ($spieler as $row) {
			  if ($row['name']==$formVars["playerName"] || $row['raceID']==$formVars["raceID"]) {
				  $returnMessage["doubleRegistration"]="player schon im spiel!";
				  return array($formVars, $returnMessage);	
			  }
		  }
		  
		  // DB INSERT QUERY
		  $sqlQuery = "INSERT INTO spieler (name, raceID, mail, punkte, bilder_gefunden) VALUES ('".$formVars["playerName"]."', '".$formVars["raceID"]."', '".$formVars["email"]."', '0', '0')";
		  
		  // DB INSERT
		  if ($result = $this->DB->query($sqlQuery, TRUE))	{
			$returnMessage=NULL;
			return array($formVars, $returnMessage);
		  } else {
			$returnMessage["db-entry"]="db entry problem!";
			return array($formVars, $returnMessage);
		  }
	   }
	   
		
	}
	
	
	//// player edit (xajax)
	public function playerBearbeiten() {
	    
		// HTML output variable
		$output = "";

		// DB SELECT * spieler
		$res = $this->DB->query("SELECT * FROM spieler");

		// eintraege nach ID sortieren
		rsort($res);

		// HTML titel table
		$output .= "<table>
						<tr id='editPlayerTableTitle'>
							<th style=\"width: 50px; height: 2em;\">id</th>
							<th style=\"width: 130px; height: 2em;\">name</th>
							<th style=\"width: 100px; height: 2em;\">raceID</th>
							<th style=\"width: 220px; height: 2em;\">mail</th>
							<th style=\"width: 100px; height: 2em;\">punkte</th>
							<th style=\"width: 140px; height: 2em;\">bilder gefunden</th>
						</tr>
					</table>\n";

		// eintraege pro seite >> alle id's auslesen / raenge fuer eintraege per $_POST uebergeben (seite neu laden) und anzeigen
		foreach($res as $row) {
			$output .= "<div id=\"entry_".$row['id']."\">
							<table>
								<tr>			
									<td style=\"width: 50px; height: 2em;\">".$row['id']."</td>\n
									<td style=\"width: 130px; height: 2em;\">".$row['name']."</td>\n
									<td style=\"width: 100px; height: 2em;\">".$row['raceID']."</td>\n
									<td style=\"width: 220px; height: 2em;\">".$row['mail']."</td>\n
									<td style=\"width: 100px; height: 2em;\">".$row['punkte']."</td>\n
									<td style=\"width: 140px; height: 2em;\">".$row['bilder_gefunden']."</td>\n

									<td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"bearbeiten\" style=\"width: auto; height: auto;\" onclick=\"xajax_editPlayer(".$row['id'].")\" /></td>\n
                                    <td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"löschen\" style=\"width: auto; height: auto;\" onclick=\"confirmSubmit(".$row['id'].",'player')\" /></td>\n
								</tr>\n
							</table>\n		
						</div>";				
		}

		// HTML ausgeben
		echo $output;
	   
	}
	
	
	//// ranking anzeigen
	public function showRanking() {
		
		// HTML output variable
		$output = "";

		// DB SELECT * spieler
		$res = $this->DB->query("SELECT * FROM spieler ORDER BY punkte DESC, bilder_gefunden ASC");
		
		// rang table
		$output .= "<div id=\"rankingTable\">
						<table>
							<tr id='rankingTableTitle'>	
								<th>RANK</th>
								<th>raceID</th>
								<th>name</th>	
								<th>POINTS</th>
								<th>places found</th>
							</tr>";	
			
		  // rang variablen
		  $rank=0;
		  $punkteBefore = 0;
		  $bilder_gefundenBefore = 0;
		  $rankEven = $rank;
		  
		  // eintraege aufliste
		  foreach($res as $row) {
						
			  $output .= "<tr>";	
			
			  // bei null punkten eine andere schriftgroesse setzen
			  if ($row['punkte']==0) {
				  $output .= "<td style='font-size:8px;'>0</td>";
			  // bei punkte gleichheit, keinen rang hinaufzaehlen
			  } else if ($punkteBefore == $row['punkte'] && $bilder_gefundenBefore == $row['bilder_gefunden']) {
				  $output .= "<td>".$rank."</td>";
				  $rankEven+=1;
			  // bei punkte gleichheit und mehr gefundenen bildern, den rang hinaufzaehlen
			  } else if ($punkteBefore == $row['punkte'] && $bilder_gefundenBefore != $row['bilder_gefunden']) {
				  if ($rankEven>0) {
					  $rank= $rank+$rankEven;
					  $rankEven=0;
				  }
				  $rank+=1;
				  $output .= "<td>".$rank."</td>";
			  } else {  
			  // bei mehr punkten als vorheriger eintrag, den rang hinaufzaehlen
				  if ($rankEven>0) {
					  $rank= $rank+$rankEven;
					  $rankEven=0;
				  }
				  
				  $rank+=1;
				  $output .= "<td>".$rank."</td>";
			  }
			  
			  $output .= "<td>".$row['raceID']."</td>";
			  $output .= "<td>".$row['name']."</td>";
			  $output .= "<td>".$row['punkte']."</td>";
			  $output .= "<td>".$row['bilder_gefunden']."</td>";
			  
			  $output .= "</tr>";	
				  
			  // rang variablen zuruecksetzen
			  $punkteBefore = $row['punkte'];
			  $bilder_gefundenBefore = $row['bilder_gefunden'];
			  
		  } 
		
		$output .= "</table>\n
				</div>";
		
		// HTML ausgeben
		echo $output;		
		
	}
	
	
	//// player registration form
	public function showPlayer_regForm($formVars) {
		$output = "              
			<div id='player_regForm'>
              
                 <form action='play.php?getRequest=newPLayer' method='post'>
                  <fieldset>
                      
                      <!--<legend>Register</legend>-->
                      <label>player: *</label>
                      <input type='text' name='playerName' value='".$formVars['playerName']."' />
                      
                      <label>RACE-ID: *</label>
                      <input type='text' name='raceID' value='".$formVars['raceID']."' />
                      
                      <label>mail:</label>
                      <input type='text' name='email' value='".$formVars['email']."' />			
                      
                      <input type='submit' value='PLAY!' />
                      
                  </fieldset>
                 </form>
              
              </div>";
			  
		  echo $output;
	}
	
	
	
}

?>