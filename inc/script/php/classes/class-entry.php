<?php

class entry {


	public function __construct() {

		$this->DB = $GLOBALS['DB'];
		$this->IMGUPLOAD = $GLOBALS['IMGUPLOAD'];

	}

	//// check data
	// text data
	public function checkDataText() {

	  // returnMessage
	  $returnMessage = "";

	  //// ueberpruefe $_POST Variablen
	  // ort
	  if (isset($_POST['bild_ort'])) {
			$formVars["bild_ort"] = htmlentities($_POST['bild_ort'], ENT_QUOTES, "UTF-8");
			if ($formVars["bild_ort"]=="") {
				$returnMessage["bild_ort"] = "bild ort eingeben!";
			}
	   }
	   // kommentar
	   if (isset($_POST['bild_kommentar'])) {
			 $formVars["bild_kommentar"] = htmlentities($_POST['bild_kommentar'], ENT_QUOTES, "UTF-8");
	   } else {
			 $formVars["bild_kommentar"] = "";
	   }
	   // aggroBonus
	   if (isset($_POST['bild_aggroBonus'])) {
			 $formVars["bild_aggroBonus"] = htmlentities($_POST['bild_aggroBonus'], ENT_QUOTES, "UTF-8");
	   } else {
			 $formVars["bild_aggroBonus"] = 0;
	   }

	   // set returnMessage NULL
	   if ($returnMessage["bild_ort"]==NULL) {
			$returnMessage=NULL;
	   }

	   //// return data
	   return array($formVars, $returnMessage);

	}

	// img data
	public function checkDataImg() {

		// img
	   if ($_FILES['bild_file']['error']==0) {
		   // check exension
		   $allowedImgExtensions = array("jpg", "JPG", "jpeg", "JPEG", "png", "PNG", "gif", "GIF");
		   if ($returnMessage["bild_file"]==NULL)  { $returnMessage["bild_file"] = $GLOBALS['IMGUPLOAD']->checkExtension($_FILES['bild_file']['name'], $allowedImgExtensions); }
		   // check imgSize
		   $allowedImgSize = 2000000; // 2'000'000 byte
		   if ($returnMessage["bild_file"]==NULL)  { $returnMessage["bild_file"] = $GLOBALS['IMGUPLOAD']->checkSize($_FILES['bild_file']['size'], $allowedImgSize); }
		   // check imgMeasure
		   $allowedImgMeasure = array("1400","1400","1600","1600"); // png width+height - other width+height
		   if ($returnMessage["bild_file"]==NULL)  { $returnMessage["bild_file"] = $GLOBALS['IMGUPLOAD']->checkMeasure($_FILES['bild_file']['tmp_name'], $allowedImgMeasure); }

		   // set bildName
		   $formVars["bild_fileName"] = htmlentities($_FILES['bild_file']['name'], ENT_QUOTES, "UTF-8");

	   } else {
		   $returnMessage["bild_file"] = $GLOBALS['IMGUPLOAD']->uploadImg_error_handling($_FILES['bild_file']['error']);
	   }
	   // img TMP
	   if (isset($_POST['bild_name'])) {
	   		$formVars["bild_name"] = htmlentities($_POST['bild_name'], ENT_QUOTES, "UTF-8");
	   } else {
		   $formVars["bild_name"] = "";
	   }

	   // set returnMessage NULL
	   if ($returnMessage["bild_file"]=="") {
			$returnMessage=NULL;
	   }

	   // return data
	   return array($formVars, $returnMessage);

	}

	//// write entry
	public function writeEntry($formVars) {

		 // timestamp herauslesen
         $res = $GLOBALS['DB']->query("SELECT timestamp FROM eintrag");

		 // eintrag schreiben falls keiner vorhanden
         if ($res==NULL) {
            if ($this->writeEntrySQL($formVars)) {
               $returnMessage = '';
               return $returnMessage;
            } else {
               // SQL query nicht möglich
               $returnMessage = "SQL Query nicht m&ouml;glich Fehler01 ";
               return $returnMessage;
			}

         // timestamp abgleichen um ueberschreibungen zu vermeiden
         } else {

      	   foreach($res as $row) {
      		   if ($formVars['timestamp'] != $row['timestamp']) {
      			   // eintrag schreiben
      			   if ($this->writeEntrySQL($formVars)) {
                     $returnMessage = '';
                     return $returnMessage;
         			} else {
	                 // SQL query nicht möglich
                     $returnMessage = "SQL Query nicht m&ouml;glich Fehler02";
                     return $returnMessage;
                  }
      		   } else {
      			   //return false; // timestamp vergleich ungueltig
      			   $returnMessage = "timestamp vergleich ungueltig";
                  return $returnMessage;
      		   }
      	   }
      	}

	}

	private function writeEntrySQL($formVars) {

	   // mysql query bild
	   $sqlQuery = "INSERT INTO bild(bildName, bildName_original, bildPfad)
		 VALUES ('".$formVars["newImg_name"]."', '".$formVars["bildName_Original"]."','".UPL_DIR."')";
	   if($res = $this->DB->query($sqlQuery, TRUE)) {
		   $this->lastId_img = $GLOBALS['DB']->mySQLiObject->insert_id; // ID des letzten Eintrages
		} else {
			return false; // SQL Query nicht möglich
		}

	   // mysql query eintrag
	   $sqlQuery = "INSERT INTO eintrag(datum, idBild, punkte, aggro_bonus, kommentar, ort, gefunden, spieler, [timestamp])
		 VALUES ('".$formVars["date"]."', '$this->lastId_img', '1', '".$formVars["bild_aggroBonus"]."', '".$formVars["bild_kommentar"]."', '".$formVars["bild_ort"]."', '0', '0', '".$formVars["timestamp"]."')";
	   if($res = $this->DB->query($sqlQuery, TRUE)) {
		   $this->lastIdEntry = $GLOBALS['DB']->mySQLiObject->insert_id; // ID des letzten Eintrages
			return true;
		} else {
			return false; // SQL Query nicht möglich
		}

	}

	//// show entries admin
	// show last entry admin
	public function showLastEntry_admin($timestamp) {
		$output = "";
		$lastEntry = $this->DB->query("SELECT * FROM eintrag WHERE timestamp={$timestamp}");

		if ($lastEntry[0]['id']!=NULL) {


			$output .= "<h3>eintrag erfolgreich!</h3>";

			$output .= "<br />\n";
			$output .= "<div id='entryAdminTableTitle'>";

			$output .= "<div style=\"width: 106px; height: 1em; display: inline-block; font-weight: bold;\">bild</div>";
			$output .= "<div style=\"width: 144px; height: 1em; display: inline-block; font-weight: bold;\">bild name</div>";
			$output .= "<div style=\"width: 58px; height: 1em; display: inline-block; font-weight: bold;\">bild ID</div>";
			$output .= "<div style=\"width: 80px; height: 1em; display: inline-block; font-weight: bold;\">datum</div>";
			$output .= "<div style=\"width: 68px; height: 1em; display: inline-block; font-weight: bold;\">punkte</div>";
			$output .= "<div style=\"width: 96px; height: 1em; display: inline-block; font-weight: bold;\">aggro bonus</div>";
			$output .= "<div style=\"width: 202px; height: 1em; display: inline-block; font-weight: bold;\">ort</div>";
			$output .= "<div style=\"width: 110px; height: 1em; display: inline-block; font-weight: bold;\">kommentar</div>";
			$output .= "<div style=\"width: 112px; height: 1em; display: inline-block; font-weight: bold;\">gefunden</div>";
			$output .= "<div style=\"width: 200px; height: 1em; display: inline-block; font-weight: bold;\">gefunden datum</div>";

			$output .= "</div>\n";


			$output .= "<div id=\"entry_".$lastEntry[0]['id']."\">";

			$output .= "<table>";
			$output .= "<tr>";

				// check img
				if ($row['idBild']!='0') {
					$imgEntry = $GLOBALS['DB']->query("SELECT * FROM bild WHERE id={$lastEntry[0]['idBild']}");
					$output .= "<td style=\"width: 100px; height: 2em;\"><img src=\"".UPL_DIR_WEB."thumbMini/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" /></td>\n";
					$output .= "<td style=\"width: 140px; height: 2em;\"><span style=\"display: block; width: 140px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;\">".$imgEntry[0]['bildName_original']."</span></td>\n";
				}

				// textData
				$output .= "<td style=\"width: 52px; height: 2em;\">".$lastEntry[0]['idBild']."</td>\n
							<td style=\"width: 78px; height: 2em;\">".$lastEntry[0]['datum']."</td>\n
				";

				if ($lastEntry[0]['aggro_bonus'] == 1) {
					$output .= "<td style=\"width: 65px; height: 2em;\">". ($lastEntry[0]['punkte'] + $lastEntry[0]['punkte']) ."</td>\n";
				} else {
					$output .= "<td style=\"width: 65px; height: 2em;\">".$lastEntry[0]['punkte']."</td>\n";
				}

				if ($lastEntry[0]['aggro_bonus'] == 1) {
					$output .= "<td style=\"width: 90px; height: 2em;\">Ja</td>\n";
				} else {
					$output .= "<td style=\"width: 90px; height: 2em;\">Nein</td>\n";
				}

				$output .=	"
							<td style=\"width: 200px; height: 2em;\">".$lastEntry[0]['ort']."</td>\n
							<td style=\"width: 105px; height: 2em;\">".$lastEntry[0]['kommentar']."</td>\n
				";

				if ($lastEntry[0]['gefunden']==1) {
					$output .= "<td style=\"width: 108px; height: 2em;\">Gefunden von: ".$lastEntry[0]['spieler']."</td>\n";
					$output .= "<td style=\"width: 108px; height: 2em;\">Gefunden am: ".$lastEntry[0]['gefunden_datum']."</td>\n";
				} else {
					$output .= "<td style=\"width: 108px; height: 2em;\">Nicht gefunden</td>\n";
					$output .= "<td style=\"width: 108px; height: 2em;\"></td>\n";
				}

				$output .= "<td style=\"width: 40px; height: 2em;\"><input type=\"button\" value=\"edit\" style=\"width: auto; height: auto;\" onClick=\"xajax_editEntry(".$lastEntry[0]['id'].")\" /></td>\n
	<td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"delete\" style=\"width: auto; height: auto;\" onClick=\"confirmSubmit(".$lastEntry[0]['id'].",'entry')\" /></td>\n";

			$output .= "</tr>\n";
			$output .= "</table>\n";


			$output .= "</div>";

			echo $output;

		}
	}

	// show all entries admin
	public function showEntries_admin() {

		$output = "";

		$res = $this->DB->query("SELECT * FROM eintrag");

		rsort($res);


			$output .= "<br />\n";
			$output .= "<div id='entryAdminTableTitle'>";

			$output .= "<div style=\"width: 106px; height: 1em; display: inline-block; font-weight: bold;\">bild</div>";
			$output .= "<div style=\"width: 144px; height: 1em; display: inline-block; font-weight: bold;\">bild name</div>";
			$output .= "<div style=\"width: 58px; height: 1em; display: inline-block; font-weight: bold;\">bild ID</div>";
			$output .= "<div style=\"width: 80px; height: 1em; display: inline-block; font-weight: bold;\">datum</div>";
			$output .= "<div style=\"width: 68px; height: 1em; display: inline-block; font-weight: bold;\">punkte</div>";
			$output .= "<div style=\"width: 96px; height: 1em; display: inline-block; font-weight: bold;\">aggro bonus</div>";
			$output .= "<div style=\"width: 202px; height: 1em; display: inline-block; font-weight: bold;\">ort</div>";
			$output .= "<div style=\"width: 110px; height: 1em; display: inline-block; font-weight: bold;\">kommentar</div>";
			$output .= "<div style=\"width: 112px; height: 1em; display: inline-block; font-weight: bold;\">gefunden</div>";
			$output .= "<div style=\"width: 200px; height: 1em; display: inline-block; font-weight: bold;\">gefunden datum</div>";

			$output .= "</div>\n";

		// Entries per Page >> Alle id's auslesen / Range fuer Eintraege per POST uebergeben (Seite neu laden) und anzeigen
		foreach($res as $row) {


			$output .= "<div id=\"entry_".$row['id']."\">";
			$output .= "<table>";
			$output .= "<tr>";

			// check img
			if ($row['idBild']!='0') {
				$imgEntry = $GLOBALS['DB']->query("SELECT * FROM bild WHERE id={$row['idBild']}");
				$output .= "<td style=\"width:100px; height:2em;\"><img src=\"".UPL_DIR_WEB."thumbMini/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" /></td>\n";
				$output .= "<td style=\"width:140px; height:2em;\"><span style=\"display: block; width: 140px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;\">".$imgEntry[0]['bildName_original']."</span></td>\n";
			}

			$output .= "<td style=\"width:52px; height:2em;\">".$row['idBild']."</td>\n
						<td style=\"width:78px; height:2em;\">".$row['datum']."</td>\n
			";

			if ($row['aggro_bonus'] == 1) {
				$output .= "<td style=\"width:65px; height:2em;\">". ($row['punkte'] + $row['punkte']) ."</td>\n";
			} else {
				$output .= "<td style=\"width:65px; height:2em;\">".$row['punkte']."</td>\n";
			}

			if ($row['aggro_bonus'] == 1) {
				$output .= "<td style=\"width: 90px; height: 2em;\">Ja</td>\n";
			} else {
				$output .= "<td style=\"width: 90px; height: 2em;\">Nein</td>\n";
			}
			$output .= "
						<td style=\"width:200px; height:2em;\">".$row['ort']."</td>\n
						<td style=\"width:105px; height:2em;\">".$row['kommentar']."</td>\n
			";

			if ($row['gefunden']==1) {
				$output .= "<td style=\"width: 108px; height: 2em;\">Gefunden von: ".$row['spieler']."</td>\n";
				$output .= "<td style=\"width: 108px; height: 2em;\">Gefunden am: ".$row['gefunden_datum']."</td>\n";
			} else {
				$output .= "<td style=\"width: 108px; height: 2em;\">Nicht gefunden</td>\n";
				$output .= "<td style=\"width: 108px; height: 2em;\"></td>\n";
			}

			$output .= "<td style=\"width: 40px; height: 2em;\"><input type=\"button\" value=\"edit\" style=\"width: auto; height: auto;\" onClick=\"xajax_editEntry(".$row['id'].")\" /></td>\n
						<td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"delete\" style=\"width: auto; height: auto;\" onClick=\"confirmSubmit(".$row['id'].",'entry')\" /></td>\n";

			$output .= "</tr>\n";
			$output .= "</table>";
			$output .= "</div>";

		}

		echo $output;

	}


	//// show all entries
	public function showFirstEntries($found) {


		if (!$GLOBALS['MOBILE_DETECT']->isMobile()) {
			$firstFive='LIMIT 0,5';
		} else {
			$firstFive='LIMIT 0,2000';
		}

		//This is your query again, the same one... the only difference is we add $max into it
		if ($found=="notFound") {

			// load on ScrollData -> Alle Eintraege auslesen
			$data = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='0'");
			$allEntries = count($data);

			// load firsFive Entries
			$res = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='0' ORDER BY ID DESC $firstFive ");

			// Eintraege ordnen (der neuste zuerst)
			rsort($res);

		} else {

			// load on ScrollData -> Alle Eintraege auslesen
			$data = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='1'");
			$allEntries = count($data);

			// load firsFive Entries
			$res = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='1' ORDER BY gefunden_datum DESC $firstFive ");
		}

		//
		$output = '';

		//
		$colorArray= array("#33ccff","#33ff33","#ff9900","#ff0066","#ff00ff","#6600ff","#00ffff","#00ff66","#99ff00","#99ff00");
		//$colorArray= array("#cccccc","#666666","#999999");
		$colorBefore="";
		$colorFont="";
		$colorBeforeHover="";
		$colorFontHover="";

		$page_break=0;

		// Eintraege aufliste
		foreach($res as $row) {

			// Datum formatieren
			//$gbDatum = DatumsWandler($row['datum']);
			//$gbDatum = substr($gbDatum, 0, 19);


			// page_break
			$page_break+=1;
			if ($page_break>=4) {
				$page_break=0;
				 $output .= "<div class=\"page_brake\"></div>";
			}

		  // randomColor
		  while ($colorBefore==$colorFont) {
			$colorFontEntry=array_rand($colorArray);
			$colorFont=$colorArray[$colorFontEntry];
		  }
		  $colorBefore=$colorFont;

		  while ($colorBeforeHover==$colorFontHover) {
			$colorFontEntry=array_rand($colorArray);
			$colorFontHover=$colorArray[$colorFontEntry];
		  }
		  $colorBeforeHover=$colorFontHover;


		  // inhalt eintragsDiv
		  if ($found=="notFound") {
		  		$output .= "<div class=\"all_entries not_found message_box\" id=\"".$allEntries."\">";
		  } else {
			    $output .= "<div class=\"all_entries found message_box\" id=\"".$allEntries."\">";
		  }
		  $allEntries-=1;

		  	// lines
			$output .= "<img class=\"all_entriesLINE\" src=\"inc/img/content/all_entriesLINE.png\" alt=\"all_entriesLINE\" />";

			// img
			if ($row['idBild']!='') {

				//
				$imgEntry = $GLOBALS['DB']->query("SELECT * FROM bild WHERE id={$row['idBild']}");

				if ($found=="notFound") {
				  //
				  $output .= "<div class=\"all_entriesIMG\">";
					  $output .= "<a href='findImg.php?getRequest=".$row['idBild']."' title=\"".$imgEntry[0]['bildName_original']."\">
								  <img src=\"".UPL_DIR_WEB."thumb/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" />
								  </a>";
				  $output .= "</div>";
				} else {
				 //
				  $output .= "<div class=\"all_entriesIMG\">";
					  $output .= "<img src=\"".UPL_DIR_WEB."thumb_deschavu/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" />";
				  $output .= "</div>";

				}
			}

			// text
			//$output .= "<div class=\"all_entriesTEXT\" onmouseover='ChangeColor(this.className, true)'>";
				if ($found=="notFound") {
					$output .= "<div class=\"all_entriesTEXT\">";
						$output .= "<p><a style='color:".$colorFont."; font-size:68px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>bildNr. ".$row['idBild']."</a></p>";
						$output .= "<p><a style='color:".$colorFont."; font-size:35px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>".setDate01($row['datum'])."</a></p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p><a style='color:".$colorFont."; font-size:75px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>POINTS: ". ($row['punkte'] + $row['punkte']) ."</a></p>";
						} else {
							$output .= "<p><a style='color:".$colorFont."; font-size:75px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>POINTS: ".$row['punkte']."</a></p>";
						}
						$output .= "<p style='width:450px;'><a style='color:".$colorFont."; font-size:28px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>".$row['kommentar']."</a></p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p><a style='color:".$colorFont."; font-size:32px; margin: 10px 0 6px 0; display: block;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>AGGRO BONUS!!!</a></p>";
						}
				} else {
					$output .= "<div class=\"all_entriesTEXT found\">";
						$output .= "<p style='color:".$colorFont."; font-size:42px;'>bildNr. ".$row['idBild']."</p>";
						$output .= "<p style='color:".$colorFont."; font-size:22px;'>".setDate01($row['datum'])."</p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p style='color:".$colorFont."; font-size:46px;'>POINTS: ". ($row['punkte'] + $row['punkte']) ."</p>";
						} else {
							$output .= "<p style='color:".$colorFont."; font-size:46px;'>POINTS: ".$row['punkte']."</p>";
						}
						$output .= "<p style='color:".$colorFont."; font-size:12px; width:700px;'>".$row['kommentar']."</p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p style='color:".$colorFont."; font-size:18px; margin: 4px 0 6px 0;'>AGGRO BONUS!!!</p>";
						}
						$output .= "<p style='color:".$colorFont."; font-size:25px;'>GEFUNDEN VON: ".$row['spieler']."</p>";
						$output .= "<p style='color:".$colorFont."; font-size:12px;'>ORT: ".$row['ort']."</p>";
				}
			$output .= "</div>";

		 	$output .= "</div>";


		}

		$output .= "<div id='last_msg_loader'></div>";

		echo $output;
	}

	//
	public function loadEntries($found) {


		$last_msg_id=$_GET['last_msg_id'];
		//var_dump($last_msg_id);
		//$sql=mysql_query("SELECT * FROM messages WHERE mes_id < '$last_msg_id' ORDER BY mes_id DESC LIMIT 5");


		$nextFive='LIMIT 5,5';

		//This is your query again, the same one... the only difference is we add $max into it
		if ($found=="notFound") {

			// load on ScrollData -> Alle Eintraege auslesen
			$data = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='0'");
			$allEntries = count($data);
			$allEntriesDiff = $allEntries-$last_msg_id+1;
			//var_dump($allEntriesDiff);

			$nextFive='LIMIT '.$allEntriesDiff.',5';

			$query="SELECT * FROM eintrag WHERE gefunden='0' ORDER BY ID DESC $nextFive";
			$res = $GLOBALS['DB']->query($query);

			// Eintraege ordnen (der neuste zuerst)
			rsort($res);

		} else {

			// load on ScrollData -> Alle Eintraege auslesen
			$data = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='1'");
			$allEntries = count($data);
			$allEntriesDiff = $allEntries-$last_msg_id+1;
			//var_dump($allEntriesDiff);
			//exit;
			/*if ($last_msg_id<=0) {
				exit;
			}*/

			$nextFive='LIMIT '.$allEntriesDiff.',5';

			$query="SELECT * FROM eintrag WHERE gefunden='1' ORDER BY gefunden_datum DESC $nextFive";
			$res = $GLOBALS['DB']->query($query);
		}

		//
		$output = '';

		//
		$colorArray= array("#33ccff","#33ff33","#ff9900","#ff0066","#ff00ff","#6600ff","#00ffff","#00ff66","#99ff00","#99ff00");
		//$colorArray= array("#cccccc","#666666","#999999");
		$colorBefore="";
		$colorFont="";
		$colorBeforeHover="";
		$colorFontHover="";

		$page_break=0;

		$entryNumber=count($res);

		//
		$nextDivID=$last_msg_id-1;

		// Eintraege aufliste
		foreach($res as $row) {

			// page_break
			$page_break+=1;
			if ($page_break>=4) {
				$page_break=0;
				 $output .= "<div class=\"page_brake\"></div>";
			}

		  // randomColor
		  while ($colorBefore==$colorFont) {
			$colorFontEntry=array_rand($colorArray);
			$colorFont=$colorArray[$colorFontEntry];
		  }
		  $colorBefore=$colorFont;

		  while ($colorBeforeHover==$colorFontHover) {
			$colorFontEntry=array_rand($colorArray);
			$colorFontHover=$colorArray[$colorFontEntry];
		  }
		  $colorBeforeHover=$colorFontHover;

		  // inhalt eintragsDiv
		  if ($found=="notFound") {
				$output .= "<div class=\"all_entries not_found message_box\" id=\"".$nextDivID."\">";
		  } else {
				$output .= "<div class=\"all_entries found message_box\" id=\"".$nextDivID."\">";
		  }
		  $nextDivID-=1;

		  	// lines
			$output .= "<img class=\"all_entriesLINE\" src=\"inc/img/content/all_entriesLINE.png\" alt=\"all_entriesLINE\" />";

			// img
			if ($row['idBild']!='') {

				//
				$imgEntry = $GLOBALS['DB']->query("SELECT * FROM bild WHERE id={$row['idBild']}");

				if ($found=="notFound") {
				  //
				  $output .= "<div class=\"all_entriesIMG\">";
					  $output .= "<a href='findImg.php?getRequest=".$row['idBild']."' title=\"".$imgEntry[0]['bildName_original']."\">
								  <img src=\"".UPL_DIR_WEB."thumb/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" />
								  </a>";
				  $output .= "</div>";
				} else {
				 //
				  $output .= "<div class=\"all_entriesIMG\">";
					  $output .= "<img src=\"".UPL_DIR_WEB."thumb_deschavu/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" />";
				  $output .= "</div>";

				}
			}

			// text
			//$output .= "<div class=\"all_entriesTEXT\" onmouseover='ChangeColor(this.className, true)'>";
				if ($found=="notFound") {
					$output .= "<div class=\"all_entriesTEXT\">";
						$output .= "<p><a style='color:".$colorFont."; font-size:68px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>bildNr. ".$row['idBild']."</a></p>";
						$output .= "<p><a style='color:".$colorFont."; font-size:35px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>".setDate01($row['datum'])."</a></p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p><a style='color:".$colorFont."; font-size:75px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>POINTS: ". ($row['punkte'] + $row['punkte']) ."</a></p>";
						} else {
							$output .= "<p><a style='color:".$colorFont."; font-size:75px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>POINTS: ".$row['punkte']."</a></p>";
						}
						$output .= "<p style='width:450px;'><a style='color:".$colorFont."; font-size:28px;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>".$row['kommentar']."</a></p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p><a style='color:".$colorFont."; font-size:32px; margin: 10px 0 6px 0; display: block;' onmouseover='this.style.color=\"".$colorFontHover."\"' onmouseout='this.style.color=\"".$colorFont."\"' href='findImg.php?getRequest=".$row['idBild']."'>AGGRO BONUS!!!</a></p>";
						}
				} else {
					$output .= "<div class=\"all_entriesTEXT found\">";
						$output .= "<p style='color:".$colorFont."; font-size:42px;'>bildNr. ".$row['idBild']."</p>";
						$output .= "<p style='color:".$colorFont."; font-size:22px;'>".setDate01($row['datum'])."</p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p style='color:".$colorFont."; font-size:46px;'>POINTS: ". ($row['punkte'] + $row['punkte']) ."</p>";
						} else {
							$output .= "<p style='color:".$colorFont."; font-size:46px;'>POINTS: ".$row['punkte']."</p>";
						}
						$output .= "<p style='color:".$colorFont."; font-size:12px; width:700px;'>".$row['kommentar']."</p>";
						if ($row['aggro_bonus'] == 1) {
							$output .= "<p style='color:".$colorFont."; font-size:18px; margin: 4px 0 6px 0;'>AGGRO BONUS!!!</p>";
						}
						$output .= "<p style='color:".$colorFont."; font-size:25px;'>GEFUNDEN VON: ".$row['spieler']."</p>";
						$output .= "<p style='color:".$colorFont."; font-size:12px;'>ORT: ".$row['ort']."</p>";
				}
			$output .= "</div>";

		 	$output .= "</div>";


		}

		$last_msg_id="";

		echo $output;

	}



	public function refreshEntries() {

		// date
		$dateNow=date("Y-m-d");

		//
		$res = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='0'");

		//
		foreach($res as $row) {
			$diff = daysDifference02($dateNow,setDate02($row['datum']));
			//var_dump($diff);
			//exit;
			if ($diff/7 > 1) {
				//
				$sqlQuery = "UPDATE eintrag SET punkte='".((int)($diff/7) + 1)."' WHERE id='".$row["id"]."'";
				$result = $this->DB->query($sqlQuery);

				// Ergebnis prüfen
				if($result === false) {
					// Fehler in der Datenbank
					return false;
				}

			}
		}

	}


	// show avaiable points
	public function showAvailablePoints() {
		$res = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE gefunden='0'");

		$availablePoints = 0;

		foreach($res as $row) {
			if ($row['aggro_bonus'] == 1) {
				// with aggro
				$availablePoints += $row['punkte'] + $row['punkte'];
			} else {
				// without aggro
				$availablePoints +=$row['punkte'];
			}
		}

		return $availablePoints;
	}







	// falls ein Fehler beim eintragen passiert, alle relevanten eintraege und files dazu loeschen
	public function error_cleanUp($eintragResultatID, $eintragDaten, $bildFile, $soundFile) {

	   // lokale Variablen
	   $eintragAlle = array();

	   // daten ueber text- bild-  und soundeintrag holen
	   $res = $GLOBALS['DB']->query("SELECT * FROM eintrag WHERE ID='$eintragResultatID'");
	   foreach($res as $row) {
   	   array_push($eintragAlle, $row['idText']);
   	   array_push($eintragAlle, $row['idBild']);
   	   array_push($eintragAlle, $row['idSound']);
   	}

	   // check imgFile / sndFile -> ev. eintraege und files loeschen // loeschen text / eintrag

	   // sndFile + sndEintrag
	   if ($soundFile!=null) {

   		//This applies the function to our file
         list($ext, $unExt) = findexts($soundFile, true);

         //This takes the random number (or timestamp) you generated and adds a . on the end, so it is ready of the file extension to be appended.
         $newFileName = $unExt . $eintragDaten[7]. ".";

   		$sndpath = UPL_DIR_SND.$newFileName.$ext;
   		// Falls Bild existiert aus dem Verzeichnis löschen
   		if (file_exists($sndpath)) {
   			unlink($sndpath);
   		} else {
   		   echo 'Konnte SndFile nicht loeschen';
   		   // log eintrag!!!
   		   return false;
   		}

   		// Sound aus DB loeschen
	      $GLOBALS['DB']->query("DELETE FROM sound WHERE ID='$eintragAlle[2]'");
	   }

	   // imgFile + imgEintrag
	   if ($bildFile!=null) {

   		//This applies the function to our file
         list($ext, $unExt) = findexts($bildFile, true);

         //This takes the random number (or timestamp) you generated and adds a . on the end, so it is ready of the file extension to be appended.
         $newFileName = $unExt . $eintragDaten[7]. ".";

   		$imgpathOriginal = UPL_DIR_IMG.$newFileName.$ext;
   		$imgpathThumb = UPL_DIR_IMG_THUMB.$newFileName.$ext;

   		// Falls Bild existiert aus dem Verzeichnis löschen
   		if (file_exists($imgpathOriginal)) {

   		   //sleep(500);
   			unlink($imgpathOriginal);

   		} else {
   		   echo 'Konnte ImgFile Original nicht loeschen';
   		   // log eintrag!!!
   		   return false;
   		}
   		// Falls Bild existiert aus dem Verzeichnis löschen
   		if (file_exists($imgpathThumb)) {

   			unlink($imgpathThumb);

   		} else {
   		   echo 'Konnte ImgFile Thumbnail nicht loeschen';
   		   // log eintrag!!!
   		   return false;
   		}

         // Bild aus DB loeschen
         $GLOBALS['DB']->query("DELETE FROM bild WHERE ID='$eintragAlle[1]'");
	   }

	   // textEintrag loeschen
	   if ($eintragAlle[0]!=null) {
	      $GLOBALS['DB']->query("DELETE FROM text WHERE ID='$eintragAlle[0]'");
	   }

	   // eintrag selbst loeschen
	   $GLOBALS['DB']->query("DELETE FROM eintrag WHERE ID='$eintragResultatID'");
	}








}

?>
