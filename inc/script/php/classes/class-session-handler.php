<?php

class sessionHandlerzh {

	public function __construct() {
		
		$this->DB = $GLOBALS['DB'];
	
		// Den SessionHandler auf die Methoden
		// dieser Klasse setzen
		session_set_save_handler(
			array($this, '_open'),
			array($this, '_close'),
			array($this, '_read'),
			array($this, '_write'),				
			array($this, '_destroy'),
			array($this, '_gc'));
			
		// Session starten
		session_start();
		
		// Ab PHP-Version 5.0.5
		//session_write_close();
		register_shutdown_function('session_write_close');
		
		//var_dump($sesID);
		//exit;
		
		
		  // Session als Gast eröffnen, wenn kein 
		  // Benutzereintrag vorhanden ist.
		  /*
		  if ( !isset($_SESSION['username']) ) {
				  $_SESSION['username'] = 'Gast';
		  }
		  if ( !isset($_SESSION['access_right_position']) ) {
				  $_SESSION['access_right_position'] = 0;
		  }
		  */

		
	}
	
	
	public function _open($path, $name) {
		return true;
	}
	
	
	public function _close() {
		// Ruft den Garbage Collector auf
		$this->_gc(0);
		return true;
	}
	
	
	public function _read($sesID) {
		$sessionStatement = "SELECT * FROM sessions WHERE sessionID='$sesID'";
		$result = $this->DB->query($sessionStatement);
		
		//var_dump($result);
		
		if($result == false) {
			// Fehler bei der Verbindung mit der Datenbank
			return '';
		}
		
		if(count($result) > 0) {
			// Es wurde ein Datensatz gefunden
			return $result[0]['value'];
		} else {
			// Es wurde KEIN Datensatz gefunden
			return '';
		}
	}
	
	
	public function _write($sesID, $data) {
		// Nur schreiben, wenn Daten übergeben werden
		if($data == null) {
			return true;
		}
		
		// Statement um eine bestehende Sitzung zu aktualisieren
		$sessionStatement = "UPDATE sessions SET lastUpdated='".time()."', value='$data' WHERE sessionID='$sesID'";
		
		$result = $this->DB->query($sessionStatement);
		
		// Ergebnis prüfen
		if($result === false) {
			// Fehler in der Datenbank
			return false;
		}
		
		if($this->DB->mySQLiObject->affected_rows) {
			// Bestehende Session wurde aktualisiert
			return true;
		}
		
		// Anderenfalls muss eine neue Session erstellt werden
		$sessionStatement = "INSERT INTO sessions(sessionID, lastUpdated, start, value) VALUES ('$sesID', '".time()."', '".time()."', '$data')";
		
		$result = $this->DB->query($sessionStatement);
		
		// Ergebnis zurückgeben
		return $result;
	}
	
	
	public function _destroy($sesID) {
		$sessionStatement = "DELETE FROM sessions WHERE sessionID='$sesID'";
		
		$result = $this->DB->query($sessionStatement);
		
		// Ergebnis zurückgeben, d.h. true oder false
		return $result;
	}
	
	public function _gc($life) {
		// Zeitraum, nach dem eine Session abgelaufen ist
		$sessionLife = strtotime("-10 minutes");
		$sessionStatement = "DELETE FROM sessions WHERE lastUpdated < $sessionLife";
		$result = $this->DB->query($sessionStatement);
		
		// Ergebnis zurückgeben
		return $result;

	}

}

?>