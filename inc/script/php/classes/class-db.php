<?php

class DB{

	// Konstruktor: Verbindung aufbauen/kontrollieren
	public function __construct()
	{

		// Objekt erstellen
		$this->mySQLiObject = new mysqli(DB_SERVER, DB_USER, DB_PASSWORD, DB_NAME);

		// Fehlerausgabe, falls keine Verbindung möglich
		if(mysqli_connect_errno()){
			echo"Keine Verbindung zum MySQL-Server möglich.";
			die();
		}
	}


	// Dekonstruktor: Verbindung beenden
	public function __destruct(){
		$this->mySQLiObject->close();
	}


	// Anfrage/Ausgabe
	public function query($sqlQuery, $resultset = FALSE)
	{
		// Letztes SQL-Statement
		$this->lastQuery = $sqlQuery;

		$result = $this->mySQLiObject->query($sqlQuery);

		// Ergebnis zurückgeben

		if($resultset == true)
		{
			$this->lastQuery = $result;

			// Status setzen
			if($result == false)
			{
				$this->lastStatus = false;
			}
			else
			{
				$this->lastStatus = true;
			}

			return $result;
		}

		$return = $this->makeArrayResult($result);
		return $return;
	}


	// Ergebnis in Array umwandeln

	private function makeArrayResult($ResultObj)
	{
		if($ResultObj === false)
		{
			// Fehler aufgetreten
			$this->lastStatus = false;
			return false;
		}
		elseif($ResultObj === true)
		{
			// Abfrage Ok!
			$this->lastStatus = true;
			return true;
		}
		elseif($ResultObj->num_rows == 0)
		{
			// Kein Ergebnis
			$this->lastStatus = true;
			return array();
		}
		else
		{
			$array = array();
			while($line = $ResultObj->fetch_array())
			{
				array_push($array, $line);
			}

			// Status setzen
			$this->lastStatus = true;

			// Array zurückgeben
			return $array;
		}

	}


	//
	public function escapeString($subString) {

		$escapedString = $this->mySQLiObject->real_escape_string($subString);
		return $escapedString;

	}


	// Fehlermeldung ausgeben

	public function lastSQLError()
	{
		return $this->MySQLiObj->error;
		//return $this->mySQLiObject->error;
	}


}

?>
