<?php

class login {
	
	public function __construct(){
		
		$this->DB = $GLOBALS['DB'];
		
	}
	
	
	public function checkLoginData() {
				
	   if (isset($_POST['benutzerName']) && ($_POST['password'])) {
		   $benutzerName = $this->DB->escapeString(trim($_POST['benutzerName']));
		   $password = $this->DB->escapeString(trim($_POST['password']));
	   } else {
	      return;
	   }
		
		$sqlQuery = "SELECT benutzerName, passwort FROM admin WHERE benutzerName='$benutzerName' AND passwort=MD5('$password')";
			
		if ($result = $this->DB->query($sqlQuery, TRUE)) {
			
			if ($result->num_rows == 1) {
				
				//var_dump($result);
				//exit;
				
				// Session_id neu generieren
				session_regenerate_id(true);
				
				// Daten des Benutzer in die Session eintragen
				$_SESSION['username'] = $benutzerName;
				
				// In der Session merken, dass der User eingeloggt ist !
            	$_SESSION["login"] = 1;
				
				// set sessionID
				$_SESSION["sessionID"] = session_id();
				
				// Schreiben der aktuellen IP und dem Datum des Logins in die Datenbank
				$sqlQuery = "UPDATE admin SET last_ip = '" .$_SERVER["REMOTE_ADDR"]. "', last_login = NOW() WHERE benutzerName = '".$benutzerName."'";
                $this->DB->query($sqlQuery);
				
				return true;
				
			} else {

				return false;
				
			}
		} else {
			
			return false;
			
		}
			
	}
	
	
	
	public function userBearbeiten() {
	   
		$output = "";

		$res = $this->DB->query("SELECT * FROM admin");

		rsort($res);

		// Entries per Page >> Alle id's auslesen / Range fuer Eintraege per POST uebergeben (Seite neu laden) und anzeigen
		foreach($res as $row) {
			$output .= "<div id=\"entry_".$row['id']."\">";
			$output .= " <table>";
			$output .= " <tr>";				
			$output .= "<td style=\"width: 20px; height: 2em;\">".$row['id']."</td>\n
						<td style=\"width: 100px; height: 2em;\">".$row['benutzerName']."</td>\n
						<td style=\"width: 100px; height: 2em;\">(Passwort)</td>\n
						<td style=\"width: 100px; height: 2em;\">".$row['email']."</td>\n
			";

			$output .= "<td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"edit\" style=\"width: auto; height: auto;\" onclick=\"xajax_editUser(".$row['id'].")\" /></td>\n
<td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"delete\" style=\"width: auto; height: auto;\" onclick=\"confirmSubmit(".$row['id'].",'user')\" /></td>\n";

			$output .= "</tr>\n";
			$output .= "</table>\n";				
			$output .= "</div>";				
		}

		echo $output;
	   
	}
		
}

?>