<?php

class findImg {

	public function __construct(){

		$this->DB = $GLOBALS['DB'];

	}

	public function checkFindImgData() {

  	  // returnMessage
	  $returnMessage = "";

	  //// ueberpruefe $_POST Variablen
	  // img
	  if (isset($_POST['imgID'])) {
			$formVars["imgID"] = htmlentities($_POST['imgID'], ENT_QUOTES, "UTF-8");
	   } else {
			$formVars["imgID"]=NULL;
	   }
	  // player
	  if (isset($_POST['player'])) {
			$formVars["player"] = htmlentities($_POST['player'], ENT_QUOTES, "UTF-8");
			if ($formVars["player"]=="noOption") {
				$returnMessage["player"] = "player eingeben!";
			}
	   }
	   // ort
	   if (isset($_POST['ort'])) {
			 $formVars["ort"] = htmlentities($_POST['ort'], ENT_QUOTES, "UTF-8");
			if ($formVars["ort"]=="") {
				$returnMessage["ort"] = "ort eingeben!";
			}
	   }

	   	// spam checkboxes
//		if ($_POST['emailCheck01']==true || $_POST['emailCheck02']!='unchecked' || $_POST['emailCheck03']=='checked') {
//			$returnMessage["spam"] = "checkboxen nicht anw&auml;hlen!";
//		}

	   if ($returnMessage!=NULL) {
			//// return data
			return array($formVars, $returnMessage);
	   } else {
		  // send mail
		  $sqlQuery = "SELECT * FROM spieler WHERE name='".$formVars['player']."'";
		  $playerData = $GLOBALS['DB']->query($sqlQuery);

		  $formVars['raceID']=$playerData[0]['raceID'];
		  $formVars['mail']=$playerData[0]['mail'];

			//// return data
			return array($formVars, NULL);

	   }

	}

	public function sendFindImgMail($mail_empfaenger, $formVars) {

		//

		//
		$org_var = "deschaVUE;";
		$subject = "deschaVü - finde bild!";

		//
		$mssg="";
		$mssg.= "deschaV&uuml;-Player hat das bildNr.".$formVars["imgID"]." gefunden!\r\n\r\n";
		$mssg.= "PLAYER DATA\r\n";
		$mssg.= "player: ".$formVars['player']."\r\n";
		$mssg.= "raceID: ".$formVars['raceID']."\r\n";
		if ($formVars['mail']!="") {
			$FromSender = $formVars['mail'];
			$mssg.= "mail: ".$formVars['mail']."\r\n\r\n";
		} else {
			$FromSender = "noreply@noreply.net";
			$mssg.= "\r\n";
		}
		$mssg.= "L&Ouml;SUNG\r\n";
		$mssg.= "ort: ".$formVars['ort']."\r\n";

		if (sendMail($mail_empfaenger, $FromSender, $org_var, $subject, $mssg)) {
			header("Location: email_sent.php");
			exit;
		} else {
			header("Location: findImg?getRequest=mailNotSent");
			exit;
		}

		//

	}



}

?>
