<?php
session_start();
// config
require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
require_once("inc/functions.inc.php");


// header
get_headerTemplate();

?>

  <div id="container">
  
      <!-- head -->
      <div id="head">
          
          <!-- logo & deko -->
          <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
          <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
          <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->
          
          <!-- adminLink -->
          <a id="loginLink" href="login.php">admin</a>
          
          <!-- menu -->
          <?php get_menuTemplate(); ?>
          
      </div>


      <?php
      $showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll

      if(isset($_GET['register'])) {
          $error = false;
          $name = trim($_POST['name']);
          $mail = trim($_POST['mail']);
          $raceID = trim($_POST['raceID']);

          if(empty($name) || empty($mail) || empty($raceID)) {
              echo 'Bitte alle Felder ausfüllen<br>';
              $error = true;
          }

          if(strlen($name) == 0) {
              echo 'Bitte ein Name angeben<br>';
              $error = true;
              }
              if (!filter_var($mail, FILTER_VALIDATE_EMAIL)) {
                  echo 'Bitte eine gültige E-Mail-Adresse eingeben<br>';
                  $error = true;
              }
              if (strlen($raceID) == 0) {
                  echo 'Bitte ein RaceID angeben<br>';
                  $error = true;
              }

              //Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
              if (!$error) {
                  $statement = $pdo->prepare("SELECT * FROM spieler WHERE mail = :mail");
                  $result = $statement->execute(array('mail' => $mail));
                  $user = $statement->fetch();

                  if ($user !== false) {
                      echo 'Diese E-Mail-Adresse ist bereits vergeben<br>';
                      $error = true;
                  }
              }

              //Keine Fehler, wir können den Nutzer registrieren
              if (!$error) {
                  $statement = $pdo->prepare("INSERT INTO spieler (name, raceID, mail) VALUES (:name, :raceID, :mail)");
                  $result = $statement->execute(array('name' => $name, 'raceID' => $raceID, 'mail' => $mail));

                  if ($result) {
                      echo 'Du wurdest erfolgreich registriert.  Allez!!</a>';
                      $showFormular = false;
                  } else {
                      echo 'Beim Abspeichern ist leider ein Fehler aufgetreten<br>';
                  }
              }
          }

      if($showFormular) {
          ?>

          <form action="?register=2" method="post">
              Name:<br>
              <input type="text" size="40" maxlength="250" name="name"><br><br>

              dini E-Mail:<br>
              <input type="email" size="40" maxlength="250" name="mail"><br><br>

              RaceID:<br>
              <input type="number"  name="raceID"><br>
              <br>
              <input type="submit" value="Spielerin regestrieren">
          </form>

          <?php
      } //Ende von if($showFormular)
      ?>
          
          </div>
    
    	</div>  
	
    </div>

<?php

	// footer
	get_footerTemplate();

?>