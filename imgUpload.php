<?php
session_start();

// config
require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
require_once("inc/functions.inc.php");


//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();



    // set form timestamp
    $formVars['timestamp'] = time();

    // pruefen ob ein REQUEST vorhanden ist
    if (isset($_REQUEST['getRequest'])) {
        $getRequest=newInput($_REQUEST['getRequest']);
    }


	// reset form -> delete tmpIMG if there
	if (isset($_POST['reset']) && isset($_POST['bild_name'])) {
		$tmpIMG_thump = UPL_DIR."tmp/thumb/".$_POST['bild_name'];
		$tmpIMG_thumpMini = UPL_DIR."tmp/thumbMini/".$_POST['bild_name'];
		$tmpIMG_original = UPL_DIR."tmp/original/".$_POST['bild_name'];
		$tmpIMG_thumbDeschavu = UPL_DIR."tmp/thumb_deschavu/".$_POST['bild_name'];
		unlink($tmpIMG_thump);
		unlink($tmpIMG_thumpMini);
		unlink($tmpIMG_original);
		unlink($tmpIMG_thumbDeschavu);
	}

	// check and write newEntry data
	if (isset($_REQUEST['action']) && $getRequest == 'newEntry') {

		// checkText entries
		list($formVars, $returnMessage) = $ENTRY->checkDataText();

		if (!isset($_POST['bild_name'])) {
			// checkImg entry
			list($formVars02, $returnMessage02) = $ENTRY->checkDataImg();

			//
			$formVars = array_merge($formVars, $formVars02);
			if ($returnMessage!=NULL && $returnMessage02!=NULL) {
				$returnMessage = array_merge($returnMessage, $returnMessage02);
			} else if ($returnMessage==NULL) {
				$returnMessage = $returnMessage02;
			}
		}

		// set timestamp and date for entry
		$formVars['date'] = date("Y-m-d H:i:s");
		if (isset($_POST['timestamp'])) {
			$formVars['timestamp'] = htmlentities($_POST['timestamp'], ENT_QUOTES, "UTF-8");
		}
		// set originalIMG_name for entry
		if (isset($_POST['bildName_Original'])) {
			$formVars['bildName_Original'] = htmlentities($_POST['bildName_Original'], ENT_QUOTES, "UTF-8");
		} else {
			$formVars['bildName_Original'] = $_FILES['bild_file']['name'];
		}

		// write
		if ($returnMessage==NULL) {

			//
			$ENTRY->refreshEntries();

			// 1. make entry (upload img / write db-data)
			if (!isset($_POST['bild_name'])) {
				// generate fileName with timestamp
				$formVars["newImg_name"] = renameTimestamp($_FILES['bild_file']['name'], $formVars['timestamp']);
				// upload img
				$returnMessage["bild_file"] = $IMGUPLOAD->uploadIMG($formVars["newImg_name"], UPL_DIR);
				if ($returnMessage["bild_file"]=="") {
					// write entry
					$returnMessage["eintrag"] = $ENTRY->writeEntry($formVars);
				}
			}

			// 2. make entry (move tmpIMG / write db-data)
			if (isset($_POST['bild_name'])) {
				$formVars["newImg_name"]=$_POST['bild_name'];
				// move img
				$currentPath = "/var/www/inc/img/uploads/tmp/";
				$destinationPath = "/var/www/inc/img/uploads/";
				$returnMessage["bild_file"] = $IMGUPLOAD->moveIMG($_POST['bild_name'], $currentPath, $destinationPath);
				if ($returnMessage["bild_file"]=="") {
					// write entry
					$returnMessage["eintrag"] = $ENTRY->writeEntry($formVars);
				}
			}

			// entry good -> show new entry and empty form
			if ($returnMessage["bild_file"]=="" && $returnMessage["eintrag"]=="") {
				header("Location: imgUpload.php?getRequest=showEntry&timestamp=".$formVars['timestamp']."");
			}

		} else if ($returnMessage['bild_ort']!="" && $returnMessage["bild_file"]==NULL) {
			// check refresh with timestamp
			// save img temporarily
			if (!isset($_POST['bild_name'])) {
				$formVars["newImg_name"] = renameTimestamp($_FILES['bild_file']['name'], $formVars['timestamp']);
				$uploadPath = UPL_DIR.'tmp/';
				$returnMessage["bild_file"] = $IMGUPLOAD->uploadIMG($formVars["newImg_name"], $uploadPath);
			} else {
				$formVars["newImg_name"] = htmlentities($_POST['bild_name'], ENT_QUOTES, "UTF-8");
			}
		}
	}

    get_headerTemplate();
	?>

    <div id='containerAdmin'>

          <!-- head -->
          <div id="head">

              <!-- logo & deko -->
              <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
              <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>

              <a id="loginLink" href="login.php">admin</a>


          </div>

        <?php


		// adminMenu
		get_adminMenu();

		?>

    <div id='contentAdmin'>
   	<div id="admin_newEntry">

      <h1>neuer eintrag</h1>

		<!-- 1. entry form -->
		<div id="admin_entryForm">

         <form enctype="multipart/form-data" action="imgUpload.php?getRequest=newEntry" method="post" accept-encoding="UTF-8">
         <fieldset>

      	   	<label class="admin_entryFormOrt" for="bild_ort">bild ort *</label>
         	<input class="admin_entryFormOrt" type="text" name="bild_ort" value="<?php echo $formVars["bild_ort"] ?>" />

            <br />

            <label class="admin_entryFormKommentar" for="bild_kommentar">bild kommentar</label>
            <input class="admin_entryFormKommentar" type="text" name="bild_kommentar" value="<?php echo $formVars["bild_kommentar"] ?>" />

            <br />

            <label class="admin_entryFormAggroBonus" for="bild_aggroBonus">aggro bonus</label>
            <input class="admin_entryFormAggroBonus" type="checkbox" name="bild_aggroBonus" value="1" <?php if ($formVars["bild_aggroBonus"] == 1) { echo 'checked=checked'; } ?>/>

            <br />
            <input class="admin_entryFormTimestamp" type="hidden" name="timestamp" value="<?php echo $formVars['timestamp'] ?>" />

            <br />

            <?php if ($returnMessage=="" || $returnMessage['bild_file']!="") { ?>
                <label for="bild_file">bild hochladen *</label>
                <p>(JPG - PNG - GIF &lowast; max. 1MB)</p>
                <input class="admin_entryFormBild" type="file" name="bild_file" />
            <?php } else if ($returnMessage!="" && $returnMessage['bild_file']=="" && $getRequest != 'showEntry') {
				$imgName = (string)$formVars["newImg_name"]; ?>
				<input class="admin_entryFormBild" type="hidden" name="bild_name" value="<?php echo $imgName ?>" />
                <input class="admin_entryFormBild" type="hidden" name="bildName_Original" value="<?php echo $_FILES['bild_file']['name'] ?>" />
                <img id="admin_tmpImg" src="<?php echo UPL_DIR_WEB.'tmp/thumbMini/'.$imgName ?>" alt="tmp image" />
			<?php } ?>

            <br />

         	<input id="admin_entryFormButton_submit" type="submit" name="action" value="upload!" />
            <input id="admin_entryFormButton_reset" type="submit" name="reset" value="abbrechen!" />


          </fieldset>
          </form>

          </div>


          <?php if ($returnMessage!="") { ?>

          <div id="admin_entryError">
              <?php

              foreach ($returnMessage as $value) {
                  echo "<p>".$value."</p>";
              }

              ?>
          </div>

          <?php }

          // 2. check and show newEntry
          if ($getRequest == 'showEntry') {
              if (isset($_REQUEST['getRequest'])) {
                  $lastTimestamp=newInput($_REQUEST['timestamp']);
              }

			  $ENTRY->showLastEntry_admin($lastTimestamp);

          }


          ?>
</div>
      </div>
      </div>

    </div>

	<?php

	// footer
	get_footerTemplate();

?>
