<?php
// config
require_once("inc/script/php/config.php");

// header
get_headerTemplate();

// loadOnScroll
$last_msg_id=$_GET['last_msg_id'];
$action=$_GET['action'];

if ($action <> "get") {

?>

    <div id="container">

        <!-- head -->
        <div id="head">

            <!-- logo & deko -->
            <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
            <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
            <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->


            <!-- adminLink -->
            <a id="loginLink" href="login.php">admin</a>

            <!-- menu -->
            <?php get_menuTemplate(); ?>

        </div>


        <div id="content">
          <?php

       // show avaiable points
       //       $output="";
       //       $output.="<div id='avaiablePoints'>";
       //       $output.="<p>Available Points: <span>".$ENTRY->showAvailablePoints()."</span></p>";
       //       $output.="</div>";
       //       echo $output;

              // list entries
              $ENTRY->showFirstEntries("notFound");

          ?>
        </div>


    </div>

<?php get_footerTemplate();

} else {

	if (!$GLOBALS['MOBILE_DETECT']->isMobile()) {
		// list entries
		$ENTRY->loadEntries("notFound");
	}

}

?>
