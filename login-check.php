<?php 

// config
require_once("inc/script/php/config.php");

// Authentifizierung überprüfen
if($LOGIN->checkLoginData() == TRUE) {
	
	header("Location: ".HTTP_ROOT."admin.php"); /* Browser umleiten */

   /* Stellen Sie sicher, dass der nachfolgende Code nicht ausgefuehrt wird, wenn
      eine Umleitung stattfindet. */
   exit;
	
} else {
	
	header("Location: ".HTTP_ROOT."login.php?getRequest=login_false"); /* Browser umleiten */
	exit;
	
}

?>

