<?php
session_start();

//phpinfo();

require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
require_once("inc/functions.inc.php");


$user = check_user();
get_headerTemplate();
?>

	   <div id='containerAdmin'>
       
      <!-- head -->
      <div id="head">
          
          <!-- logo & deko -->
          <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
          <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
          <!-- <a id="badge" href=""><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->
          
          <!-- adminLink -->
          <a id="loginLink" href="login.php">admin</a>
          
          <!-- menu -->
          
      </div>
      <?php
		
		// adminMenu
		get_adminMenu();
	
	// eintraege zum bearbeiten anzeigen
	?>
    	<div id='contentAdmin'>
    	<h1>eintr&auml;ge</h1>
    <?php
	
  	$ENTRY->showEntries_admin(); 
	
	?>
    	</div>
    	</div>

    <?php
	// footer
	get_footerTemplate();
?>