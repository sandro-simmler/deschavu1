<?php
session_start();
require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
require_once("inc/functions.inc.php");

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
get_headerTemplate();
?>

   <div id='containerAdmin'>

          <!-- head -->
          <div id="head">

              <!-- logo & deko -->
              <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
              <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
              <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->

              <!-- adminLink -->
              <a id="loginLink" href="login.php">admin</a>

          </div>

        <?php get_adminMenu(); ?>

   		<div id='contentAdmin'>
   		<div id="admin_regEdit">

          <h1>admin bearbeiten</h1>

            <table class="table">
                <tr>
                    <th>ID</th>
                    <th>Vorname</th>
                    <th>Nachname</th>
                    <th>Email</th>
                    <th>Löschen</th>
                </tr>

                <?php
                $statement = $pdo->prepare("SELECT * FROM users ORDER BY id DESC");
                $result = $statement->execute();
                while($row = $statement->fetch()) {
                    echo "<tr>";
                    echo "<td>".$row['id']."</td>";
                    echo "<td>".$row['vorname']."</td>";
                    echo "<td>".$row['nachname']."</td>";
                    echo "<td>".$row['email']."</td>";
                    echo "<td><a href='deleteuser.php?id=".$row['id']."'>Delete</a></td>";
                    echo "</tr>";
                }
                ?>
            </table>

   		</div>
   		</div>

    </div>

	<?php

	// footer
	get_footerTemplate();


?>
