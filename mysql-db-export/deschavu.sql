-- MySQL dump 10.13  Distrib 8.0.22, for Linux (x86_64)
--
-- Host: localhost    Database: deschavu
-- ------------------------------------------------------
-- Server version	8.0.22-0ubuntu0.20.04.3

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bild`
--

DROP TABLE IF EXISTS `bild`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `bild` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bildName` varchar(255) NOT NULL DEFAULT '',
  `bildName_original` varchar(255) NOT NULL,
  `bildPfad` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=742 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bild`
--

LOCK TABLES `bild` WRITE;
/*!40000 ALTER TABLE `bild` DISABLE KEYS */;
INSERT INTO `bild` VALUES (741,'1607778863.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(740,'1607778852.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(739,'1607778780.flieger_gelb.jpg','flieger_gelb.jpg','/var/www/inc/img/uploads/'),(738,'1607778454.flieger_gelb.jpg','flieger_gelb.jpg','/var/www/inc/img/uploads/'),(737,'1607778454.flieger_gelb.jpg','flieger_gelb.jpg','/var/www/inc/img/uploads/'),(736,'1607777910.265px-zurichmontage.jpg','265px-ZurichMontage.jpg','/var/www/inc/img/uploads/'),(735,'1604248480.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(734,'1603028045.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(733,'1603028045.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(732,'1602610463.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(731,'1602606968.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(730,'1602606715.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(729,'1602605770.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(728,'1602605770.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(727,'1602604009.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(726,'1602604009.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(725,'1602598954.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(724,'1602596501.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(723,'1602596501.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(722,'1602596501.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(721,'1602596501.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(720,'1602596501.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(719,'1602596501.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(718,'1602594504.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(717,'1602594504.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/'),(716,'1602594367.veloflyer.jpg','veloflyer.jpg','/var/www/inc/img/uploads/');
/*!40000 ALTER TABLE `bild` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eintrag`
--

DROP TABLE IF EXISTS `eintrag`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `eintrag` (
  `id` int NOT NULL AUTO_INCREMENT,
  `datum` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `idBild` int NOT NULL DEFAULT '0',
  `punkte` int NOT NULL,
  `aggro_bonus` tinyint(1) NOT NULL,
  `ort` varchar(255) NOT NULL,
  `kommentar` text NOT NULL,
  `gefunden` int NOT NULL,
  `gefunden_datum` date NOT NULL,
  `spieler` varchar(255) NOT NULL,
  `timestamps` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=519 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eintrag`
--

LOCK TABLES `eintrag` WRITE;
/*!40000 ALTER TABLE `eintrag` DISABLE KEYS */;
INSERT INTO `eintrag` VALUES (1,'2013-11-01 00:06:38',735,372,0,'haldenstrasse 157','',0,'2014-03-05','ADI',NULL),(2,'2013-11-01 00:06:38',736,372,0,'haldenstrasse 157','',0,'2014-03-05','ADI',NULL);
/*!40000 ALTER TABLE `eintrag` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sessions` (
  `sessionID` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `lastUpdated` int NOT NULL DEFAULT '0',
  `start` int NOT NULL DEFAULT '0',
  `value` blob NOT NULL,
  PRIMARY KEY (`sessionID`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sessions`
--

LOCK TABLES `sessions` WRITE;
/*!40000 ALTER TABLE `sessions` DISABLE KEYS */;
/*!40000 ALTER TABLE `sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `spieler`
--

DROP TABLE IF EXISTS `spieler`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `spieler` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `raceID` varchar(255) NOT NULL,
  `mail` varchar(255) NOT NULL,
  `punkte` int DEFAULT '0',
  `bilder_gefunden` int DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `spieler`
--

LOCK TABLES `spieler` WRITE;
/*!40000 ALTER TABLE `spieler` DISABLE KEYS */;
INSERT INTO `spieler` VALUES (3,'rehe','123','reto_hefel@yahoo.de',30,18),(4,'esco','955','atreju15@hotmail.com',63,26),(5,'tmx','666','tmxklr@gmx.ch',56,26),(6,'xy','999','test@test.ch',0,0),(7,'Henne','1979','hwhpeetz@yahoo.de',0,0),(8,'herr spr&uuml;ngli','1836','herrspruengli@web.de',163,37),(9,'Bock','#815','Christophbock@hotmail.com',28,9),(10,'siril','4182','sikjuu@gmail.com',255,79),(11,'suuprmario','853','clownesk@gmail.com',4,2),(12,'Hari Laeder','770','Shop@fastbikeparts.ch',1,1),(13,'Leo','888','leo.nie@gmx.ch',4,3),(15,'ma&#039;ki','875','info@maki3000.net',6,2),(17,'strahli!','60','welcome@formlabor.com',9,9),(18,'chris','5','lepoivron@gmail.com',1,0),(20,'cherry','#7','tobishare@gmx.ch',28,7),(23,'timtom','275','mail@studioluminaire.com',0,0),(24,'Paul E. Neu','10619','union.balkonia@gmail.com',0,0),(25,'razvan','672','nautilus@riseup.net',0,0),(26,'Gian','983','gian-andri@bluewin.ch',33,3),(27,'mireso1','159','miriam.sourlier@me.com',666666,25),(50,'test','123','rs@test.ch',0,0),(51,'test','12345','tes@df.ch',200000,2000),(52,'Simmler Sandro','434','sandro.simmler@gmail.com',0,0),(53,'blasdf','342','asdf@dfdsafg.ch',0,0);
/*!40000 ALTER TABLE `spieler` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `test`
--

DROP TABLE IF EXISTS `test`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `test` (
  `id` int NOT NULL AUTO_INCREMENT,
  `bildName` varchar(255) NOT NULL DEFAULT '',
  `bildName_original` varchar(255) NOT NULL,
  `bildPfad` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=558 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `test`
--

LOCK TABLES `test` WRITE;
/*!40000 ALTER TABLE `test` DISABLE KEYS */;
INSERT INTO `test` VALUES (557,'556','556','556'),(556,'','','/var/www/inc/img/uploads/');
/*!40000 ALTER TABLE `test` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `passwort` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vorname` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `nachname` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'sandro.simmler@veloblitz.ch','$2y$10$mo4QF/8gxXdG8.SNGy8FAetrx6C5LQxp3WHtXM5LTUx4DOEV6CMWy','sandro','simmler','2020-09-13 09:56:56','2020-09-13 09:56:56'),(8,'sandro@sa.ch','$2y$10$ku/QpGKOTg1QpYfnQaU0a.zOIu7fZiuIhapo5GMrXWhVBKLNu1J8W','test','tsdf','2020-09-14 17:30:55','2020-09-14 17:30:55'),(9,'test@test.com','$2y$10$LLlPcOx6cLdeIcwT.bIW5.7.4zJq2v9cXdMyJk8HGubPGTE7EhNSi','test','test','2020-09-23 13:35:33','2020-09-23 13:35:33');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-12-30 15:22:45
