<?php 

// config
require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
//require_once("inc/functions.inc.php");
// header
get_headerTemplate();

?>

  <div id="container">
  
      <!-- head -->
      <div id="head">
          
          <!-- logo & deko -->
          <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
          <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
          <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->
          
          <!-- adminLink -->
          <a id="loginLink" href="login.php">admin</a>
          
          <!-- menu -->
          <?php get_menuTemplate(); ?>
          
      </div>
      

      <div id="content">    
      
      	<div id="ranking">
        
            <h1>RANKING</h1>

            <table class="table">
                <tr>
                    <th>Rang</th>
                    <th>name</th>
                    <th>RaceID</th>
                    <th>punkte</th>
                    <th>gefunden</th>
                </tr>

                <?php
                $statement = $pdo->prepare("SELECT * FROM spieler ORDER BY punkte DESC");
                $result = $statement->execute();
                $count = 1;
                while($row = $statement->fetch()) {
                    echo "<tr>";
                    echo "<td>".$count++."</td>";
                    echo "<td>".$row['name']."</td>";
                    echo "<td>".$row['raceID']."</td>";
                    echo "<td>".$row['punkte']."</td>";
                    echo "<td>".$row['bilder_gefunden']."</td>";
                    echo "</tr>";
                }
                ?>
            </table>

        </div>
      </div>
      
  </div>

<?php get_footerTemplate(); ?>