<?php

use Jaxon\Jaxon;                      // Use the jaxon core class
use Jaxon\Response\Response;          // and the Response class

// editEntry
function editEntry($id) {
	
	// Xajax-Antwort-Objekt instanzieren
    $objResponse = new Response();

    // Datenbankabfrage eintrag
	$sqlQuery = "SELECT * FROM eintrag WHERE id='$id'";
		
	if ($result = $GLOBALS['DB']->query($sqlQuery)) {	
	   	   
		$newContent = "<form id=\"updateForm\" style=\"padding: 1em; background: #c5c5c5;\" action=\"javascript:void(null);\" onsubmit=\"submitUpdate();\">";
		
		foreach($result as $res) {
			
			$newContent .= "<label>ort:</label><br />";
			$newContent .= "<textarea name=\"ort\">".$res['ort']."</textarea><br /><br />";
			
			$newContent .= "<label>kommentar:</label><br />";
			$newContent .= "<textarea name=\"kommentar\">".$res['kommentar']."</textarea><br /><br />";
			
			$newContent .= "<label>aggro bonus:</label><br />";
			if ($res["aggro_bonus"] == 1) {
				$newContent .= "<input type=\"checkbox\" name=\"aggro_bonus\" value=\"1\" checked=\"checked\"; /><br /><br />";
			} else {
				$newContent .= "<input type=\"checkbox\" name=\"aggro_bonus\" value=\"1\" /><br /><br />";
			}		
			
			$player = $GLOBALS['DB']->query("SELECT * FROM spieler");
			$newContent.= "<label>gefunden von:</label><br />";
			$newContent.= "<p>
							<select name='gefunden'>
							<option value='noOption'>&nbsp;</option>";
							foreach($player as $row) {
								if ($row['name']==$res['spieler']) {
									$newContent.="<option selected='selected' value='".$row['name']."'>".$row['name']."</option>";
								} else {
									$newContent.="<option value='".$row['name']."'>".$row['name']."</option>";
								}
							}
			$newContent.= "</select>
							</p><br />";
								  			
			//$newContent .= "<input  type='checkbox' name=\"gefunden\" value='".$res['gefunden']."' />";			
			
			// Datenbankabfrage bild
			$sqlQueryIMG = "SELECT * FROM bild WHERE id='".$res['idBild']."'";
			if ($resultIMG = $GLOBALS['DB']->query($sqlQueryIMG)) {
				foreach($resultIMG as $resIMG) {
				
					
					$newContent .= "<label>bild:</label><br />";
					//$newContent .= "<p>(JPG - PNG - GIF &lowast; max. 1MB)</p>";
					$newContent .= "<img src=\"".UPL_DIR_WEB."thumbMini/".$resIMG['bildName']."\" alt\"".$resIMG['bildName_original']."\" /><br /><br />";
					//$newContent .= "<input type=\"file\" name=\"bild_file\" /><br />";
					
					
		   			$newContent .= "<label>bild name:</label><br />";
					$newContent .= "<textarea name=\"bildName_original\">".$resIMG['bildName_original']."</textarea><br />";
									
				}
			}
			
			$newContent .= "<input type=\"submit\" id=\"submitButton\" value=\"Update\" />";			
			$newContent .= "<input type=\"hidden\" name=\"id\" value=\"".$res['id']."\" />";					
		}
			$newContent .= "</form>";
			
	} else {
		$newContent = "Keine Anfrage per Ajax möglich.";
	}	
		
    // Inhalt dem Anwort-Objekt zuweisen
    $objResponse->addAssign("entry_".$id, "innerHTML", $newContent);
	
    // Antwort-Objekt zurückgeben
    return $objResponse;
}


function processDataEntries($aFormValues) {
	
	// Xajax-Antwort-Objekt instanzieren
    $objResponse = new Response();

    //
	$eintragQuery="SELECT * FROM eintrag WHERE id='".$aFormValues['id']."'";
	if ($result = $GLOBALS['DB']->query($eintragQuery)) {
 	   $idBild = $result[0]['idBild'];
	   if ($result[0]['aggro_bonus'] == 1) {
		   $bildPunkte = $result[0]['punkte'] + $result[0]['punkte'];	
	   } else {
	   		$bildPunkte = $result[0]['punkte'];   
	   }
	   $bildGefunden = $result[0]['gefunden'];
	   $bildSpieler = $result[0]['spieler'];
 	} else {
 		$newContent = "Keine Anfrage per Ajax möglich.";
 	}
	
	
	$aFormValues['gefunden']=htmlentities($aFormValues['gefunden'], ENT_QUOTES, "UTF-8");
	//$aFormValues['gefunden']=htmlspecialchars($aFormValues['gefunden'], ENT_QUOTES);
	
	// 
	if ($aFormValues['gefunden']!="noOption") {
		$spielerQuery="SELECT * FROM spieler WHERE name='".$aFormValues['gefunden']."'";
		if($result = $GLOBALS['DB']->query($spielerQuery)) {
		   $idSpieler = $result[0]['id'];
		   $spielerName = $result[0]['name'];
		   $spielerPunkte = $result[0]['punkte'];
		   $spielerBilder_gefunden = $result[0]['bilder_gefunden'];
		} else {
			$newContent = "Keine Anfrage per Ajax möglich.";
		}
	} 
	
	// bild von anderem spieler gefunden oder bilder gefunden rueckgaengig
	if ($bildSpieler!=$spielerName) {
		$spielerQueryALT="SELECT * FROM spieler WHERE name='".$bildSpieler."'";
		if($result = $GLOBALS['DB']->query($spielerQueryALT)) {
			$punkteMinus_spielerALT=$result[0]['punkte']-$bildPunkte;
			$bilderMinus_spielerALT=$result[0]['bilder_gefunden']-1;
		}
		
		$updateQuery = "UPDATE spieler SET punkte='$punkteMinus_spielerALT', bilder_gefunden='$bilderMinus_spielerALT' WHERE name='$bildSpieler'";   
		
		if(!$result = $GLOBALS['DB']->query($updateQuery)) {
			// Keine Aktualisierung möglich
			$newContent = "Keine Aktualisierung möglich.";
		}
		
	}  
	if ($bildGefunden==0 || $bildSpieler!=$spielerName) {
		$neuePunkte=$bildPunkte+$spielerPunkte;
		$neueBilder_gefunden=$spielerBilder_gefunden+1;
	} else {
		$neuePunkte=$spielerPunkte;
		$neueBilder_gefunden=$spielerBilder_gefunden;
	}
	
	
    if(isset($aFormValues['ort'])) { $aFormValues['ort'] = htmlentities($aFormValues['ort'], ENT_QUOTES, "UTF-8"); }
    if(isset($aFormValues['kommentar'])) { $aFormValues['kommentar'] = htmlentities($aFormValues['kommentar'], ENT_QUOTES, "UTF-8"); }
	if(isset($aFormValues['aggro_bonus'])) { $aFormValues['aggro_bonus'] = htmlentities($aFormValues['aggro_bonus'], ENT_QUOTES, "UTF-8"); }
    if(isset($aFormValues['bildName_original'])) { $aFormValues['bildName_original'] = htmlentities($aFormValues['bildName_original'], ENT_QUOTES, "UTF-8"); }
    if(isset($aFormValues['gefunden'])) { $aFormValues['gefunden'] = htmlentities($aFormValues['gefunden'], ENT_QUOTES, "UTF-8"); }
	  
	
	  
	// Aktualisierte Daten in DB schreiben	
	if ($aFormValues['gefunden']!="noOption") {
       $updateQuery = "UPDATE eintrag, bild, spieler SET eintrag.ort='".$aFormValues['ort']."', eintrag.kommentar='".$aFormValues['kommentar']."', eintrag.aggro_bonus='".$aFormValues['aggro_bonus']."', eintrag.gefunden='1', eintrag.gefunden_datum=NOW(), eintrag.spieler='$spielerName', bild.bildName_original='".$aFormValues['bildName_original']."', spieler.punkte='$neuePunkte', spieler.bilder_gefunden='$neueBilder_gefunden' WHERE eintrag.id='".$aFormValues['id']."' AND bild.id='$idBild' AND spieler.id='$idSpieler'"; 
    } else {
	   $updateQuery = "UPDATE eintrag, bild SET eintrag.ort='".$aFormValues['ort']."', eintrag.kommentar='".$aFormValues['kommentar']."', eintrag.aggro_bonus='".$aFormValues['aggro_bonus']."', eintrag.gefunden='0', eintrag.gefunden_datum='0000-00-00', eintrag.spieler='0', bild.bildName_original='".$aFormValues['bildName_original']."' WHERE eintrag.id='".$aFormValues['id']."' AND bild.id='$idBild'"; 
	   
	   	  //var_dump($updateQuery);
	      //exit;
    } 

	if($result = $GLOBALS['DB']->query($updateQuery)) {
		$newContent = showSingleEntry($aFormValues['id']);
	} else {
		// Keine Aktualisierung möglich
		$newContent = "Keine Aktualisierung möglich.";
	}
	
	/*
	// delete old img AND upload new one
	if (isset($aFormValues['bild_file'])) {
	}
	*/
	
    $objResponse->addAssign("entry_".$aFormValues['id'], "innerHTML", $newContent);

	return $objResponse;

}

function showSingleEntry($id) {
	
	$sqlQuery = "SELECT * FROM eintrag WHERE id='$id'";

	$res = $GLOBALS['DB']->query($sqlQuery);

	foreach($res as $row) {
		$output .= "<div id=\"entry_".$id."\">";
		
		//$output .= "<h3>letzer eintrag</h3>";
		$output .= "<table>";
		$output .=  "<tr>";	
		
		// check img
		if ($row['idBild']!='0') {
			$imgEntry = $GLOBALS['DB']->query("SELECT * FROM bild WHERE id={$row['idBild']}");
			$output .= "<td style=\"width: 100px; height: 2em;\"><img src=\"".UPL_DIR_WEB."thumbMini/".$imgEntry[0]['bildName']."\" alt=\"".$imgEntry[0]['bildName_original']."\" /></td>\n";
			$output .= "<td style=\"width: 140px; height: 2em;\"><span style=\"display: block; width: 140px; text-overflow: ellipsis; white-space: nowrap; overflow: hidden;\">".$imgEntry[0]['bildName_original']."</span></td>\n";   		
		}
		
		$output .= "<td style=\"width: 52px; height: 2em;\">".$row['idBild']."</td>\n
					<td style=\"width: 78px; height: 2em;\">".$row['datum']."</td>\n
		";
		
		if ($row['aggro_bonus'] == 1) {
			$output .= "<td style=\"width:65px; height:2em;\">". ($row['punkte'] + $row['punkte']) ."</td>\n";	
		} else {
			$output .= "<td style=\"width:65px; height:2em;\">".$row['punkte']."</td>\n";
		}
		
		if ($row['aggro_bonus'] == 1) {
          	$output .= "<td style=\"width: 90px; height: 2em;\">Ja</td>\n";
        } else {
          	$output .= "<td style=\"width: 90px; height: 2em;\">Nein</td>\n";
        }
		
		$output .=  "			
					<td style=\"width: 200px; height: 2em;\">".$row['ort']."</td>\n
					<td style=\"width: 105px; height: 2em;\">".$row['kommentar']."</td>\n
		";
		
		if ($row['gefunden']==1) {
			$output .= "<td style=\"width: 108px; height: 2em;\">Gefunden von: ".$row['spieler']."</td>\n";
			$output .= "<td style=\"width: 108px; height: 2em;\">Gefunden am: ".$row['gefunden_datum']."</td>\n";
		} else {
			$output .= "<td style=\"width: 108px; height: 2em;\">Nicht gefunden</td>\n";
			$output .= "<td style=\"width: 108px; height: 2em;\"></td>\n";
		}
			
			
		$output .= "<td style=\"width: 40px; height: 2em;\"><input type=\"button\" value=\"edit\" style=\"width: auto; height: auto;\" onClick=\"jaxon_editEntry(".$row['id'].")\" /></td>\n
<td style=\"width: 50px; height: 2em;\"><input type=\"button\" value=\"delete\" style=\"width: auto; height: auto;\" onClick=\"jaxon_deleteEntry(".$row['id'].")\" /></td>\n";
		$output .= "</tr>\n";
		$output .= "</table>\n";
		$output .= "</div>";
		
	}
	
	return $output;
}

function deleteEntry($id) {
	
	// Xajax-Antwort-Objekt instanzieren
	$objResponse = new Response();

    // ueberpruefen ob files geloescht werden sollen
	$idBild = $GLOBALS['DB']->query("SELECT idBild FROM eintrag WHERE id={$id}");
	$idBild = $idBild[0]['idBild'];
   
	if ($idBild!=0) {
	    // thumb und bild loeschen
		$bildName = $GLOBALS['DB']->query("SELECT bildName FROM bild WHERE id={$idBild}");
      
   		$pfadBildOriginal = UPL_DIR."original/".$bildName[0]['bildName'];
   		$pfadBildThumb = UPL_DIR."thumb/".$bildName[0]['bildName'];
		$pfadBildThumbDeschavu = UPL_DIR."thumb_deschavu/".$bildName[0]['bildName'];
		$pfadBildThumbMini = UPL_DIR."thumbMini/".$bildName[0]['bildName'];
		
		// Falls Bild existiert aus dem Verzeichnis löschen
		if (file_exists($pfadBildOriginal)) {
			unlink($pfadBildOriginal);
		} else {
		    $newContent = "Konnte ImgFile nicht loeschen!";
		}
		// Falls Bild existiert aus dem Verzeichnis löschen
		if (file_exists($pfadBildThumb)) {
			unlink($pfadBildThumb);
		} else {
		    $newContent = "Konnte ImgFile Thumbnail nicht loeschen!";
		}
		// Falls Bild existiert aus dem Verzeichnis löschen
		if (file_exists($pfadBildThumbDeschavu)) {
			unlink($pfadBildThumbDeschavu);
		} else {
		    $newContent = "Konnte ImgFile Thumbnail nicht loeschen!";
		}
		// Falls Bild existiert aus dem Verzeichnis löschen
		if (file_exists($pfadBildThumbMini)) {
			unlink($pfadBildThumbMini);
		} else {
		    $newContent = "Konnte ImgFile ThumbnailMini nicht loeschen!";
		}
	
		// delete DB entry bild
		$deleteQueryBild = "DELETE FROM bild WHERE id='$idBild'";
		if($result = $GLOBALS['DB']->query($deleteQueryBild)) {  
			$newContent = "";
		  } else {
			$newContent = "Eintrag konnte nicht gelöscht werden!";
		}
	} 
		
	// delete DB entry
	$deleteQueryEintrag = "DELETE FROM eintrag WHERE id='$id'";
	
	if($result = $GLOBALS['DB']->query($deleteQueryEintrag)) {  
		$newContent = "";
	} else {
		$newContent = "Eintrag konnte nicht gelöscht werden!";
	}
		
    $objResponse->addAssign("entry_".$id, "innerHTML", $newContent);
    
    return $objResponse;
	
}







// editUser
function editUser($id) {
   
   	// Xajax-Antwort-Objekt instanzieren
      $objResponse = new Response();

    $sqlQuery = "SELECT * FROM admin WHERE id='$id'";

   	if($result = $GLOBALS['DB']->query($sqlQuery)) {	

   		$newContent = "<form id=\"updateForm\" style=\"padding: 1em; background: #c5c5c5;\" action=\"javascript:void(null);\" onsubmit=\"submitUpdate();\">";

   		foreach($result as $res) {
   			$newContent .= "<label>Benutzer Name</label><br />";
   			$newContent .= "<textarea name=\"benutzerName\">".$res['benutzerName']."</textarea><br />";

   			$newContent .= "<label>Passwort</label><br />";
   			$newContent .= "<textarea name=\"passwort\">(Hier neues Passwort eintragen)</textarea><br />";

   			$newContent .= "<label>Email</label><br />";
   			$newContent .= "<textarea name=\"email\">".$res['email']."</textarea><br />";
            
            $newContent .= "<input type=\"submit\" id=\"submitButton\" value=\"Update\" />";			
   			$newContent .= "<input type=\"hidden\" name=\"id\" value=\"".$res['id']."\" />";
         }
         
   		$newContent .= "</form>";
   		
   	} else {

   		$newContent = "Keine Anfrage per Ajax möglich.";
   	}

       // Inhalt dem Anwort-Objekt zuweisen
       $objResponse->addAssign("entry_".$id, "innerHTML", $newContent);

       // Antwort-Objekt zurückgeben
       return $objResponse;
}
   
   
function processDataUser($aFormValues) {
	
	// Xajax-Antwort-Objekt
    $objResponse = new Response();
   if(isset($aFormValues['benutzerName'])) { $aFormValues['benutzerName'] = htmlentities($aFormValues['benutzerName'], ENT_QUOTES, "UTF-8"); }
   if(isset($aFormValues['passwort'])) { $aFormValues['passwort'] = md5(htmlentities($aFormValues['passwort'], ENT_QUOTES, "UTF-8")); }
   if(isset($aFormValues['email'])) { $aFormValues['email'] = htmlentities($aFormValues['email'], ENT_QUOTES, "UTF-8"); }
   
	$updateQuery = "UPDATE admin SET benutzerName='".$aFormValues['benutzerName']."', passwort='".$aFormValues['passwort']."', email='".$aFormValues['email']."' WHERE id='".$aFormValues['id']."'";
	
	if($result = $GLOBALS['DB']->query($updateQuery)) {
		$newContent = showSingleUser($aFormValues['id']);
	} else {
		// Keine Aktualisierung möglich
		$newContent = "Keine Aktualisierung möglich.";
	}
	
    $objResponse->addAssign("entry_".$aFormValues['id'], "innerHTML", $newContent);

	return $objResponse;

}


function showSingleUser($id) {
	
	$sqlQuery = "SELECT * FROM admin WHERE id='$id'";

	$res = $GLOBALS['DB']->query($sqlQuery);

	foreach($res as $row){
		$output = "<table>";
		$output .= "<tr>";				
		$output .= "<td>".$row['id']."</td>\n
					<td>".$row['benutzerName']."</td>\n
					<td>(Passwort)</td>\n
					<td>".$row['email']."</td>\n
					";
					
					$output .= "<td><input type=\"button\" value=\"edit\" style=\"width: auto; height: auto;\" onClick=\"jaxon_editUser(".$row['id'].")\" /></td>\n
								<td><input type=\"button\" value=\"delete\" style=\"width: auto; height: auto;\" onClick=\"jaxon_deleteUser(".$row['id'].")\" /></td>\n";
		$output .= "</tr>\n";
		$output .= "</table>\n";
		
	}
	
	return $output;
}

function deleteUser($id) {
   
    // Xajax-Antwort-Objekt
	$objResponse = new Response();

    $deleteQueryEintrag = "DELETE FROM admin WHERE id='$id'";

	if ($result = $GLOBALS['DB']->query($deleteQueryEintrag)) {
		$newContent = "";
	} else {
		$newContent = "Eintrag konnte nicht gelöscht werden!";
	}
	
    $objResponse->addAssign("entry_".$id, "innerHTML", $newContent);
    
    return $objResponse;
	
}





// editPlayer
function editPlayer($id) {
   
   	// Xajax-Antwort-Objekt instanzieren
      $objResponse = new Response();

    $sqlQuery = "SELECT * FROM spieler WHERE id='$id'";

   	if($result = $GLOBALS['DB']->query($sqlQuery)) {	

   		$newContent = "<form id=\"updateForm\" style=\"padding: 1em; background: #c5c5c5;\" action=\"javascript:void(null);\" onsubmit=\"submitUpdate();\">";

   		foreach($result as $res) {
   			$newContent .= "<label>name</label><br />";
   			$newContent .= "<textarea name=\"name\">".$res['name']."</textarea><br />";

   			$newContent .= "<label>raceID</label><br />";
   			$newContent .= "<textarea name=\"raceID\">".$res['raceID']."</textarea><br />";

   			$newContent .= "<label>mail</label><br />";
   			$newContent .= "<textarea name=\"mail\">".$res['mail']."</textarea><br />";
			
   			$newContent .= "<label>punkte</label><br />";
   			$newContent .= "<textarea name=\"punkte\">".$res['punkte']."</textarea><br />";

   			$newContent .= "<label>bilder gefunden</label><br />";
   			$newContent .= "<textarea name=\"bilder_gefunden\">".$res['bilder_gefunden']."</textarea><br />";
            
            $newContent .= "<input type=\"submit\" id=\"submitButton\" value=\"Update\" />";			
   			$newContent .= "<input type=\"hidden\" name=\"id\" value=\"".$res['id']."\" />";
         }
         
   		$newContent .= "</form>";
   		
   	} else {

   		$newContent = "Keine Anfrage per Ajax möglich.";
   	}

       // Inhalt dem Anwort-Objekt zuweisen
       $objResponse->addAssign("entry_".$id, "innerHTML", $newContent);

       // Antwort-Objekt zurückgeben
       return $objResponse;
}
   
   
function processDataPlayer($aFormValues) {
	
	// Xajax-Antwort-Objekt
    $objResponse = new Response();

    if(isset($aFormValues['name'])) { $aFormValues['name'] = htmlentities($aFormValues['name'], ENT_QUOTES, "UTF-8"); }
   if(isset($aFormValues['raceID'])) { $aFormValues['raceID'] = htmlentities($aFormValues['raceID'], ENT_QUOTES, "UTF-8"); }
   if(isset($aFormValues['mail'])) { $aFormValues['mail'] = htmlentities($aFormValues['mail'], ENT_QUOTES, "UTF-8"); }
   if(isset($aFormValues['punkte'])) { $aFormValues['punkte'] = htmlentities($aFormValues['punkte'], ENT_QUOTES, "UTF-8"); }
   if(isset($aFormValues['bilder_gefunden'])) { $aFormValues['bilder_gefunden'] = htmlentities($aFormValues['bilder_gefunden'], ENT_QUOTES, "UTF-8"); }


	$updateQuery = "UPDATE spieler SET name='".$aFormValues['name']."', raceID='".$aFormValues['raceID']."', mail='".$aFormValues['mail']."', punkte='".$aFormValues['punkte']."', bilder_gefunden='".$aFormValues['bilder_gefunden']."' WHERE id='".$aFormValues['id']."'";
	
	if($result = $GLOBALS['DB']->query($updateQuery)) {
		$newContent = showSinglePlayer($aFormValues['id']);
	} else {
		// Keine Aktualisierung möglich
		$newContent = "Keine Aktualisierung möglich.";
	}
	
    $objResponse->addAssign("entry_".$aFormValues['id'], "innerHTML", $newContent);

	return $objResponse;

}


function showSinglePlayer($id) {
	
	$sqlQuery = "SELECT * FROM spieler WHERE id='$id'";

	$res = $GLOBALS['DB']->query($sqlQuery);

	foreach($res as $row){
		$output = "<table>";
		$output .= "<tr>";				
		$output .= "<td style=\"width: 50px; height: 2em;\">".$row['id']."</td>\n
					<td style=\"width: 130px; height: 2em;\">".$row['name']."</td>\n
					<td style=\"width: 100px; height: 2em;\">".$row['raceID']."</td>\n
					<td style=\"width: 220px; height: 2em;\">".$row['mail']."</td>\n
					<td style=\"width: 100px; height: 2em;\">".$row['punkte']."</td>\n
					<td style=\"width: 140px; height: 2em;\">".$row['bilder_gefunden']."</td>\n
					";
					
					$output .= "<td><input type=\"button\" value=\"edit\" style=\"width: 50px; height: auto;\" onclick=\"jaxon_editPlayer(".$row['id'].")\" /></td>\n
								<td><input type=\"button\" value=\"delete\" style=\"width: 50px; height: auto;\" onclick=\"jaxon_deletePlayer(".$row['id'].")\" /></td>\n";
		$output .= "</tr>\n";
		$output .= "</table>\n";
		
	}
	
	return $output;
}

function deletePlayer($id) {
   
    // Xajax-Antwort-Objekt
	$objResponse = new Response();

    $deleteQueryEintrag = "DELETE FROM spieler WHERE id='$id'";

	if ($result = $GLOBALS['DB']->query($deleteQueryEintrag)) {
		$newContent = "";
	} else {
		$newContent = "Eintrag konnte nicht gelöscht werden!";
	}
	
    $objResponse->addAssign("entry_".$id, "innerHTML", $newContent);
    
    return $objResponse;
	
}




function processForm($aFormValues) {
      
	if (array_key_exists("id",$aFormValues) && array_key_exists("ort",$aFormValues)) {
		return processDataEntries($aFormValues);
	}
	if (array_key_exists("id",$aFormValues) && array_key_exists("benutzerName",$aFormValues)) {
		return processDataUser($aFormValues);
	}
	if (array_key_exists("id",$aFormValues) && array_key_exists("name",$aFormValues)) {
		return processDataPlayer($aFormValues);
	}

}


$jaxon = Jaxon::getInstance();        // Get the core singleton object

$jaxon->register(Jaxon::USER_FUNCTION, 'editEntry($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'processDataEntries($aFormValues)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'showSingleEntry($id) '); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'deleteEntry($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'editUser($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'processDataUser($aFormValues)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'showSingleUser($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'deleteUser($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'editPlayer($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'processDataPlayer($aFormValues)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'showSinglePlayer($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'deletePlayer($id)'); // Register the function with Jaxon
$jaxon->register(Jaxon::USER_FUNCTION, 'processForm($aFormValues)'); // Register the function with Jaxon

$jaxon->processRequest();             // Call the Jaxon processing engine



?>