<?php
session_start();
require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
require_once("inc/functions.inc.php");



$user = check_user();
get_headerTemplate();
?>

   <div id='containerAdmin'>

          <!-- head -->
          <div id="head">

              <!-- logo & deko -->
              <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
              <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
              <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->

              <!-- adminLink -->
              <a id="loginLink" href="login.php">admin</a>

              <!-- menu -->

          </div>

        <?php
		get_adminMenu();
		    ?>             

   		<div id='contentAdmin'>
   		<div id="admin_regEdit">

          <h1>player bearbeiten</h1>

            <table class="table">
                <tr>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>

                <?php
                $PLAYER->playerBearbeiten();
                ?>
            </table>

   		</div>
        </div>

    </div>

	<?php
	// footer
	get_footerTemplate();
?>
