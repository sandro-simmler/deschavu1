<?php
session_start();
require_once("inc/script/php/config.php");
require_once("inc/config.inc.php");
require_once("inc/functions.inc.php");

//Überprüfe, dass der User eingeloggt ist
//Der Aufruf von check_user() muss in alle internen Seiten eingebaut sein
$user = check_user();
get_headerTemplate();
?>

   <div id='containerAdmin'>

          <!-- head -->
          <div id="head">

              <!-- logo & deko -->
              <img id="deko" src="inc/img/content/logo/deko.jpg" alt="Deko" />
              <a id="logo" href=""><img src="inc/img/content/logo/logo.jpg" alt="Logo" /></a>
              <!-- <a id="badge" href="http://deschav&uuml;.ch/"><img src="inc/img/content/logo/vol2_badge02.png" alt="Badge" /></a> -->

              <!-- adminLink -->
              <a id="loginLink" href="login.php">admin</a>

          </div>

        <?php get_adminMenu(); ?>




        <div id="contentAdmin">
            <div id="admin_register">
    <h1>neuer admin</h1>
<?php
$showFormular = true; //Variable ob das Registrierungsformular anezeigt werden soll

if(isset($_GET['register'])) {
    $error = false;
    $vorname = trim($_POST['vorname']);
    $nachname = trim($_POST['nachname']);
    $email = trim($_POST['email']);
    $passwort = $_POST['passwort'];
    $passwort2 = $_POST['passwort2'];

    if(empty($vorname) || empty($nachname) || empty($email)) {
        echo 'Bitte alle Felder ausfüllen<br>';
        $error = true;
    }

    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo 'Bitte eine gültige E-Mail-Adresse eingeben<br>';
        $error = true;
    }
    if(strlen($passwort) == 0) {
        echo 'Bitte ein Passwort angeben<br>';
        $error = true;
    }
    if($passwort != $passwort2) {
        echo 'Die Passwörter müssen übereinstimmen<br>';
        $error = true;
    }

    //Überprüfe, dass die E-Mail-Adresse noch nicht registriert wurde
    if(!$error) {
        $statement = $pdo->prepare("SELECT * FROM users WHERE email = :email");
        $result = $statement->execute(array('email' => $email));
        $user = $statement->fetch();

        if($user !== false) {
            echo 'Diese E-Mail-Adresse ist bereits vergeben<br>';
            $error = true;
        }
    }

    //Keine Fehler, wir können den Nutzer registrieren
    if(!$error) {
        $passwort_hash = password_hash($passwort, PASSWORD_DEFAULT);

        $statement = $pdo->prepare("INSERT INTO users (email, passwort, vorname, nachname) VALUES (:email, :passwort, :vorname, :nachname)");
        $result = $statement->execute(array('email' => $email, 'passwort' => $passwort_hash, 'vorname' => $vorname, 'nachname' => $nachname));

        if($result) {
            echo 'Du wurdest erfolgreich registriert. <a href="login.php">Zum Login</a>';
            $showFormular = false;

?>
    </div>
  </div>
</div>
<?php
        } else {
            echo 'Beim Abspeichern ist leider ein Fehler aufgetreten<br>';
            ?>
                </div>
              </div>
            </div>
            <?php
        }
    }
}

if($showFormular) {
    ?>
    		<div id="admin_entryForm">
    <form action="?register=1" method="post">

        <div class="form-group">
            <label for="inputVorname">Vorname: </label>
            <input type="text" id="inputVorname"  name="vorname" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="inputNachname">Nachname: </label>
            <input type="text" id="inputNachname"  name="nachname" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="inputEmail">E-Mail:</label>
            <input type="email" id="inputEmail"  name="email" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="inputPasswort">Dein Passwort:</label>
            <input type="password" id="inputPasswort" size="40"  maxlength="250" name="passwort" class="form-control" required>
        </div>

        <div class="form-group">
            <label for="inputPasswort2">Passwort wiederholen:</label>
            <input type="password" id="inputPasswort2" size="40" maxlength="250" name="passwort2" class="form-control" required>
        </div>
        <button type="submit" class="btn btn-lg btn-primary btn-block">Registrieren</button>
    </form>


            </div>
        </div>
        </div>
    </div>

<?php
}

get_footerTemplate();


?>
